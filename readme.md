HOW TO Build

=> go to folder easyswoole 
=> build docker image with command, docker build -t <image-tag>:<version-tag> .
=> if major update(version must php version update, or something big) Dockerfile inside folder easyswoole must be updated
=> all backend backend retainer_program, place on folder easyswoole/retainer_program > inside container, placed at /var/www/html/reals
=> logs of retainer_program, place on easyswoole/retainer_program/logs > inside container, placed at /var/www/html/logs

HOW TO Deploy

=> go to folder kube
=> running deployment with command, kubectl apply -f retainer-program.yaml
=> running service with command, kubectl apply -f retainer-program-service.yaml
