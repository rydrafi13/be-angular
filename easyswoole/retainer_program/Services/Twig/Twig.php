<?php
namespace Services;

require_once(__DIR__.'/vendor/autoload.php');

use Twig\Loader\FilesystemLoader;
use Twig\Environment;

class Twig{
    private $templates;

    function __construct($path) {
        $loader = new FilesystemLoader($path);
        $this->templates = new Environment($loader);
    }

    function render($fileName, $data = []) {
        return $this->templates->render($fileName, $data);
    }

}