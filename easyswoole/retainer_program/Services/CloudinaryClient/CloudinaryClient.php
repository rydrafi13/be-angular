<?php
namespace Services;

if (is_file(__DIR__ . '/vendor/autoload.php') && is_readable(__DIR__ . '/vendor/autoload.php')) {
    require_once __DIR__.'/vendor/autoload.php';
} else {
    // Fallback to legacy autoloader
    require_once __DIR__.'/autoload.php';
}

use Cloudinary\Cloudinary;
use Cloudinary\Transformation\Resize;
use Cloudinary\Transformation\Delivery;
use Cloudinary\Transformation\Quality;

class CloudinaryClient {

    private static $instance = null;
    private static $cloudinary = null;

    private $errorCode = false;
    private $errorMsg = false;

	private final function __construct() {}

    private final function __clone() {}

	private final function __wakeup() {}
	
	public static function getInstance($config) {
        try {
            if( self::$instance == null || self::$cloudinary == null) {
                self::$instance = new self;
                self::$cloudinary = new Cloudinary(
                    [
                        'cloud'=>[
                            "cloud_name" => $config['cloud_name'], 
                            "api_key" => $config['api_key'], 
                            "api_secret" => $config['api_secret']
                        ],
                        'url'=>[
                            "secure" => $config['secure'] ? true : false,
                            "sign_url" => $config['sign_url'] ? true : false,
                        ]
                    ]
                );
            }
    
            return self::$instance;
        } catch (\Exception $e) {
            return self::$instance->sendError($e->getMessage(), $e->getCode());
        }
    }

    function uploadDocument($path, $uploadOptions = []) {

        try {

            return self::$cloudinary->uploadApi()->upload(
                $path,
                $uploadOptions
            );

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }

    function uploadImage($path, $uploadOptions = [], $applyDefaultPreset = true){

        try {

            if($applyDefaultPreset){
                $uploadOptions['upload_preset'] = 'qgood_climit_eagerasync';
            }
    
            if($uploadOptions['use_preset']){
                $uploadOptions['upload_preset'] = $uploadOptions['use_preset'];
            }
            
            return self::$cloudinary->uploadApi()->upload(
                $path,
                $uploadOptions
            );

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }

    function signedImageTransformation($publicId, $uploadOptions){

        try {

            $imgTag = self::$cloudinary->imageTag($publicId);

            if (!empty($uploadOptions['resize'])) {
                $mode = $uploadOptions['resize']['mode'];
                $resize = Resize::$mode();
                $resize->width($uploadOptions['resize']['width']);
                $resize->height($uploadOptions['resize']['height']);

                $imgTag->resize($resize);
            }

            if (!empty($uploadOptions['quality'])) {
                $quality = $uploadOptions['quality'];
                $imgTag->delivery(Delivery::quality(Quality::$quality()));
            }

            return $imgTag;
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }

    private function sendError($errorMessage="", $errorCode=false)
    {
        if ($errorCode) {
            $this->errorCode = $errorCode;
        }
        if ($errorMessage) {
            $this->errorMsg = $errorMessage;
        }
        return $this->sendResult();
    }

    private function sendResult($result='')
    {
        if ($this->errorMsg !== false) {
            throw new \Exception($this->errorMsg, $this->errorCode);
        }

        return $result;
    }
}