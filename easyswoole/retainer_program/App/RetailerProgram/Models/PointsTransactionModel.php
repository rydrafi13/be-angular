<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class PointsTransactionModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dth_points_transaction";

    public $requestColumns = "_id,points,user_id,reference_no,description,remarks,status,transaction_month,item_code,process_type,process_number,process_id,created_date,updated_date,deleted_date,count";
    public $requestMapping = [
        '_id' => '$_id',
        'points'=>'$points',
        'user_id'=>'$user_id',
        'reference_no'=>'$reference_no',
        'description'=>'$description',
        'remarks'=>'$remarks',
        'status'=>'$status',
        'transaction_month'=>'$transaction_month',
        'item_code'=>'$item_code',
        'process_type'=>'$process_type',
        'process_number'=>'$process_number',
        'process_id'=>'$process_id',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }

            $pipeline = [];
            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($request['based_on'] == 'process_number') {
                $requestMatch['process_type'] = $requestMatch['process_type']?:['$in'=>['ADD','ADJ_IN','ADJ_OUT']];
                $pipeline = array_merge(
                    $pipeline,
                    [
                        [
                            '$group'=>[
                                '_id'=>'$process_number',
                                'process_number'=>['$first'=>'$process_number'],
                                'process_type'=>['$first'=>'$process_type'],
                                'total_transaction'=>['$sum'=>1],
                                'status'=>['$first'=>'$status'],
                                'created_date'=>['$first'=>'$created_date'],
                                'updated_date'=>['$first'=>'$updated_date']
                            ]
                        ]
                    ]
                );
                $sliceLen = 3;
            } elseif ($request['based_on'] == 'member') {
                $pipeline = array_merge(
                    $pipeline,
                    [
                        [
                            '$match'=>[
                                'process_type'=>['$in'=>['ADD','ADJ_IN','ADJ_OUT','SUB']],
                                'status'=>'PROCESSED'
                            ]
                        ],
                        [
                            '$group'=>[
                                '_id'=>'$user_id',
                                'point_add'=>[
                                    '$sum'=>[
                                        '$cond'=>[
                                            [
                                                '$eq'=>[
                                                    '$process_type',
                                                    'ADD'
                                                ]
                                            ],
                                            '$points',
                                            0
                                        ]
                                    ]
                                ],
                                'point_subtract'=>[
                                    '$sum'=>[
                                        '$cond'=>[
                                            [
                                                '$eq'=>[
                                                    '$process_type',
                                                    'SUB'
                                                ]
                                            ],
                                            ['$multiply'=>['$points', -1]],
                                            0
                                        ]
                                    ]
                                ],
                                'point_adj_in'=>[
                                    '$sum'=>[
                                        '$cond'=>[
                                            [
                                                '$eq'=>[
                                                    '$process_type',
                                                    'ADJ_IN'
                                                ]
                                            ],
                                            '$points',
                                            0
                                        ]
                                    ]
                                ],
                                'point_adj_out'=>[
                                    '$sum'=>[
                                        '$cond'=>[
                                            [
                                                '$eq'=>[
                                                    '$process_type',
                                                    'ADJ_OUT'
                                                ]
                                            ],
                                            ['$multiply'=>['$points', -1]],
                                            0
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            '$lookup'=>[
                                'from'=>'dtm_member',
                                'localField'=>'_id',
                                'foreignField'=>'_id',
                                'as'=>'members'
                            ]
                        ],
                        [
                            '$addFields'=>[
                                'full_name'=>['$arrayElemAt'=>['$members.full_name',0]],
                                'username'=>['$arrayElemAt'=>['$members.username',0]],
                                'id_toko'=>['$arrayElemAt'=>['$members.input_form_data.id_pel',0]],
                                'nama_toko'=>['$arrayElemAt'=>['$members.input_form_data.nama_toko',0]],
                                'nama_pemilik'=>['$arrayElemAt'=>['$members.input_form_data.nama_pemilik',0]],
                                'no_wa_pemilik'=>['$arrayElemAt'=>['$members.input_form_data.no_wa_pemilik',0]],
                                'alamat_toko'=>['$arrayElemAt'=>['$members.input_form_data.alamat_toko',0]],
                                'total_point'=>['$sum'=>['$point_add','$point_subtract','$point_adj_in','$point_adj_out']],
                                'group'=>['$arrayElemAt'=>['$members.input_form_data.description',0]],
                            ]
                        ]
                    ]
                );
                $sliceLen = 6;
            } else {
                $pipeline = array_merge(
                    $pipeline,
                    [
                        [
                            '$lookup'=>[
                                'from'=>'dtm_member',
                                'localField'=>'user_id',
                                'foreignField'=>'_id',
                                'as'=>'members'
                            ]
                        ],
                        [
                            '$addFields'=>[
                                'full_name'=>['$arrayElemAt'=>['$members.full_name',0]],
                                'username'=>['$arrayElemAt'=>['$members.username',0]]
                            ]
                        ]
                    ]
                );
            }

            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }
            $pipeline[] = ['$project' => $columnMapping];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);
            $pipeline[] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($pipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}