<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class MemberAuthorizationModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dth_member_authorization";

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBfind($filter);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }

            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}