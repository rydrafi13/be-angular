<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class ProductModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dtm_product";

    public $requestColumns = "_id,product_name,product_code,description,variation,type,tnc,category,status,weight,volume_weight,dimensions,images_gallery,created_date,updated_date,deleted_date,total_qty,product_sku,count";
    public $requestMapping = [
        '_id' => '$_id',
        'product_name'=>'$product_name',
        'product_code'=>'$product_code',
        'description'=>'$description',
        'total_qty'=>'$total_qty',
        'variation'=>'$variation',
        'type'=>'$type',
        'category'=>'$category',
        'status'=>'$status',
        'weight'=>'$weight',
        'volume_weight'=>'$volume_weight',
        'dimensions'=>'$dimensions',
        'images_gallery'=>'$images_gallery',
        'product_sku'=>'$product_sku',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }

            $pipeline = [];
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }

            $pipeline = array_merge(
                $pipeline,
                [
                    [
                        '$lookup'=>[
                            'from'=>'dtm_product_sku',
                            'let'=>['product_id'=>'$_id'],
                            'pipeline'=>[
                                [
                                    '$match'=>[
                                        '$expr'=>[
                                            '$and'=>[
                                                ['$eq'=>['$product_id','$$product_id']],
                                                ['$ne'=>['$deleted', 1]]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    '$group'=>[
                                        '_id'=>null,
                                        'total_qty'=>['$sum'=>'$qty']
                                    ]
                                ]
                            ],
                            'as'=>'product_sku'
                        ]
                    ],
                    [
                        '$lookup'=>[
                            'from'=>'dtm_evoucher',
                            'let'=>['product_id'=>'$_id'],
                            'pipeline'=>[
                                [
                                    '$match'=>[
                                        '$expr'=>[
                                            '$and'=>[
                                                ['$eq'=>['$product_id','$$product_id']],
                                                ['$eq'=>['$qty_in',1]],
                                                ['$eq'=>['$qty_out',0]],
                                                ['$eq'=>['$status','ACTIVE']],
                                                ['$gte'=>[
                                                        ['$dateToString'=>[
                                                            'format'=> '%Y-%m-%d',
                                                            'date'=> '$expiry_date',
                                                            'timezone' => '+07:00'
                                                        ]],
                                                        date('Y-m-d')
                                                        ]
                                                ],
                                                ['$ne'=>['$deleted', 1]]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    '$group'=>[
                                        '_id'=>null,
                                        'total_qty'=>['$sum'=>1]
                                    ]
                                ],
                            ],
                            'as'=>'evoucher'
                        ]
                    ],
                    [
                        '$addFields'=>[
                            'total_qty'=>[
                                '$cond'=>[
                                    'if'=>['$eq'=>['$type','e-voucher']],
                                    'then'=>[
                                        '$cond'=>[
                                            'if'=>['$gt'=>[['$size'=>'$evoucher'],0]],
                                            'then'=>['$arrayElemAt'=>['$evoucher.total_qty',0]],
                                            'else'=>0
                                        ]
                                    ],
                                    'else'=>['$arrayElemAt'=>['$product_sku.total_qty',0]]
                                ]
                            ],
                        ]
                    ]
                ]
            );
            $sliceLen = 3;

            $pipeline[] = ['$project' => $columnMapping];

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);
            $pipeline[] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($pipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            $pipeline = [];
            if (!empty($match)) {
                $pipeline[] = ['$match'=>$match];
            }

            $pipeline[] = [
                '$lookup'=>[
                    'from'=>'dtm_product_sku',
                    'let'=>['product_id'=>'$_id','product_type'=>'$type'],
                    'pipeline'=>[
                        [
                            '$match'=>[
                                '$expr'=>[
                                    '$and'=>[
                                        ['$eq'=>['$product_id','$$product_id']],
                                        ['$ne'=>['$deleted', 1]]
                                    ]
                                ]
                            ]
                        ],
                        [
                            '$lookup'=>[
                                'from'=>'dtm_evoucher',
                                'let'=>['sku_id'=>'$_id'],
                                'pipeline'=>[
                                    [
                                        '$match'=>[
                                            '$expr'=>[
                                                '$and'=>[
                                                    ['$eq'=>['$sku_id','$$sku_id']],
                                                    ['$eq'=>['$qty_in',1]],
                                                    ['$eq'=>['$qty_out',0]],
                                                    ['$eq'=>['$status','ACTIVE']],
                                                    ['$gte'=>[
                                                            ['$dateToString'=>[
                                                                'format'=> '%Y-%m-%d',
                                                                'date'=> '$expiry_date',
                                                                'timezone' => '+07:00'
                                                            ]],
                                                            date('Y-m-d')
                                                            ]
                                                    ],
                                                    ['$ne'=>['$deleted', 1]]
                                                ]
                                            ]
                                        ]
                                    ],
                                    [
                                        '$group'=>[
                                            '_id'=>null,
                                            'total_qty'=>['$sum'=>1]
                                        ]
                                    ],
                                ],
                                'as'=>'evoucher'
                            ]
                        ],
                        [
                            '$addFields'=>[
                                'qty'=>[
                                    '$cond'=>[
                                        'if'=>['$eq'=>['$$product_type','e-voucher']],
                                        'then'=>[
                                            '$cond'=>[
                                                'if'=>['$gt'=>[['$size'=>'$evoucher'],0]],
                                                'then'=>['$arrayElemAt'=>['$evoucher.total_qty',0]],
                                                'else'=>0
                                            ]
                                        ],
                                        'else'=>'$qty'
                                    ]
                                ],
                            ]
                        ]
                    ],
                    'as'=>'product_sku'
                ]
            ];

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findByProductCodeOrId($productCodeOrId, $findActive = null) {
        try {
            $this->requestColumns .= ',product_sku';
            $this->requestMapping['product_sku'] = '$product_sku';

            !is_array($productCodeOrId) ?
                $productCodeOrId = array($productCodeOrId): 
                null;

            $mapId=[];
            foreach ($productCodeOrId as $id) {
                $mapId[] = $this->convertToObjectId($id);
            }

            $mapCode = $productCodeOrId;
            $match = [
                '$or'=>[
                    ['_id'=>['$in'=>$mapId]],
                    ['product_code'=>['$in'=>$mapCode]]
                ]
            ];

            !empty($findActive)?
                $match['status'] = $findActive:
                null;

            $pipeline = [
                [
                    '$match'=>$match
                ],
                [
                    '$lookup'=>[
                        'from'=>'dtm_product_sku',
                        'let'=>['product_id'=>'$_id','product_type'=>'$type'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$and'=>[
                                            ['$eq'=>['$product_id','$$product_id']],
                                            ['$ne'=>['$deleted', 1]]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                '$lookup'=>[
                                    'from'=>'dtm_evoucher',
                                    'let'=>['sku_id'=>'$_id'],
                                    'pipeline'=>[
                                        [
                                            '$match'=>[
                                                '$expr'=>[
                                                    '$and'=>[
                                                        ['$eq'=>['$sku_id','$$sku_id']],
                                                        ['$eq'=>['$qty_in',1]],
                                                        ['$eq'=>['$qty_out',0]],
                                                        ['$eq'=>['$status','ACTIVE']],
                                                        ['$gte'=>[
                                                                ['$dateToString'=>[
                                                                    'format'=> '%Y-%m-%d',
                                                                    'date'=> '$expiry_date',
                                                                    'timezone' => '+07:00'
                                                                ]],
                                                                date('Y-m-d')
                                                                ]
                                                        ],
                                                        ['$ne'=>['$deleted', 1]]
                                                    ]
                                                ]
                                            ]
                                        ],
                                        [
                                            '$group'=>[
                                                '_id'=>null,
                                                'total_qty'=>['$sum'=>1]
                                            ]
                                        ],
                                    ],
                                    'as'=>'evoucher'
                                ]
                            ],
                            [
                                '$addFields'=>[
                                    'qty'=>[
                                        '$cond'=>[
                                            'if'=>['$eq'=>['$$product_type','e-voucher']],
                                            'then'=>[
                                                '$cond'=>[
                                                    'if'=>['$gt'=>[['$size'=>'$evoucher'],0]],
                                                    'then'=>['$arrayElemAt'=>['$evoucher.total_qty',0]],
                                                    'else'=>0
                                                ]
                                            ],
                                            'else'=>'$qty'
                                        ]
                                    ],
                                ]
                            ]
                        ],
                        'as'=>'product_sku'
                    ]
                ]
            ];

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}