<?php
namespace App\RetailerProgram\ViewModels\PackingListViewModel;

use App\RetailerProgram\Models\PackingListModel;
use App\GeneralData\ViewModels\ReportViewModel;

function findAllReport($arguments, $thisViewModel) {
    $search = $arguments[0];
    $order_by = $arguments[1];
    $page = $arguments[2];
    $limit_per_page = $arguments[3];
    $requested_columns = $arguments[4];
    $download = $arguments[5];

    try {
        $PackingListModel = new PackingListModel();

        // utk urutan output
        if (!isset($requested_columns)) {
            $requested_columns = "_id,packing_no,reference_no,courier_code,courier_name,delivery_method,delivery_service,status,delivery_list,total_price,total_quantity,delivery_date";
        }
        $requested_columns = explode(",", $requested_columns);

        $PackingListModel->requestColumns .= ',delivery_list,total_price,total_quantity';
        $PackingListModel->requestMapping = array_merge(
            $PackingListModel->requestMapping,
            [
                '_id'=>0,
                'delivery_list'=>'$delivery_list',
                'total_price'=>'$total_price',
                'total_quantity'=>'$total_quantity'
            ]
        );

        if ($download) {
            $result_db = $PackingListModel->findAllReport($search, $order_by, $page, null, $requested_columns);
            $result_db = json_decode(json_encode($result_db['result']['values']), 1);

            if (count($result_db) == 0) {
                $thisViewModel->sendResult('Empty result founded, No need to convert it as Excel / Csv Spread sheet', 400);
            } else {
                $ReportViewModel = new ReportViewModel();

                $result = $ReportViewModel->generateReport($result_db, 'PACKING_LIST_REPORT');
            }
            
        } else {
            $result = $PackingListModel->findAllReport($search, $order_by, $page, $limit_per_page, $requested_columns);
            $result['result']['values'] = json_decode(json_encode($result['result']['values']), 1);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}