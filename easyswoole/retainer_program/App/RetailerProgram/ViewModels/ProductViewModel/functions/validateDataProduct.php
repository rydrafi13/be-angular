<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

function validateDataProduct($arguments, $thisViewModel) {
    $productData = $arguments[0];
    try {
        $productType = ["product","voucher","e-voucher","gold","top-up"];

        if (!in_array($productData['type'], $productType)) {
            $thisViewModel->sendError("invalid product type {$productData['type']}", 400);
        }

        if ($productData['type'] == 'product' || $productData['type'] == 'voucher' || $productData['type'] == 'gold') {
            $volume = $thisViewModel->calculateVolumetricWeight($productData['dimensions']['length'], $productData['dimensions']['width'], $productData['dimensions']['height']);

            $productData = array_merge($productData, $volume);
            
            if (!preg_match("/^[0-9]+([.][0-9]+)?$/i", strval($productData['weight']))) {
                $thisViewModel->sendError("Weight must be a real number", 400);
            }
            $productData['weight'] = doubleval($productData['weight']);

            if ($productData['type'] == 'voucher' || $productData['type'] == 'voucher') {
                $productData['variation'] = [];
            }

        } elseif ($productData['type'] == 'e-voucher' || $productData['type'] == 'top-up') {
            unset($productData['dimensions']);
            unset($productData['weight']);
            unset($productData['variation']);
        }
        
        $productSKU = [];
        if (empty($productData['variation'])) {
            if (!preg_match("/^[0-9]+$/i", strval($productData['product_value']))) {
                $thisViewModel->sendError("base value must be a real number", 400);
            }

            if (!preg_match("/^[0-9]+$/i", strval($productData['qty']))) {
                $thisViewModel->sendError("quantity must be a real number", 400);
            }
            $productSKU[] = [
                // 'sku_code'=>$productData['product_code'].'-001',
                'sku_code'=>$productData['product_code'],
                'sku_price'=>$productData['product_price'],
                'sku_value'=>intval($productData['product_value']),
                'product_id'=>null,
                'product_code'=>$productData['product_code'],
                'variant'=>null,
                'qty'=>$productData['qty'] ?: 0,
                'image_gallery'=>null,
                'status'=>$productData['status']
            ];
        } else {
            $variant = [];
            foreach ($productData['variation'] as $key => $value) {
                if (!is_array($value)) 
                    $thisViewModel->sendError("product variation {$key} is invalid", 400);

                $temp = $value;
                $buff = [];
                foreach ($value as $sk => $sval) {
                    if (!is_string($sval))
                        $thisViewModel->sendError("invalid value on product variation {$key}", 400);
                    unset($temp[$sk]);
                    if(in_array($sval, $temp))
                        $thisViewModel->sendError("repeated value \"{$sval}\" on product variation {$key}", 400);
                 
                    if (!empty($variant)) {
                        foreach ($variant as $k => $val) {
                            $buff[$k.$sval] = array_merge($val, [$key => $sval]);
                        }
                    } else {
                        $buff[$sval] = [$key => $sval];
                    }
                }
                $variant = $buff;
            }

            if (empty($productData['product_sku']))
                $thisViewModel->sendError("product sku list must not be empty ", 400);

            foreach ($productData['product_sku'] as $key => $value) {
                $skuCode = trim(strval($value['sku_code']));
                if (empty($skuCode)) {
                    $thisViewModel->sendError("invalid sku code on product sku list {$key}", 400);
                }

                if (isset($productSKU[$value['sku_code']])) {
                    $thisViewModel->sendError("multiple sku code \"{$value['sku_code']}\" not allowed", 400);
                }

                if (!preg_match("/^[0-9]+$/i", strval($value['sku_value']))) {
                    $thisViewModel->sendError("sku value must be a real number", 400);
                }
                $value['sku_value'] = intval($value['sku_value']);

                $var = '';
                foreach ($productData['variation'] as $sk => $sval) {
                    if (empty($value['variant'][$sk]) || !is_string($value['variant'][$sk])) {
                        $thisViewModel->sendError("invalid variant {$sk} on product sku list {$key}", 400);
                    }
                    $var .= $value['variant'][$sk];
                }
                if (empty($variant[$var])) {
                    $thisViewModel->sendError("invalid product variant on product sku list {$key}", 400);
                }

                if (!preg_match("/^[0-9]+$/i", strval($value['qty']))) {
                    $thisViewModel->sendError("sku quantity must be a real number", 400);
                }

                $productSKU[$value['sku_code']] = [
                    // 'sku_code' => $productData['product_code'].'-'.$value['sku_code'].'-'.sprintf("%03d",$key+1),
                    'sku_code' => $value['sku_code'],
                    'sku_price' => $value['sku_price'],
                    'sku_value' => $value['sku_value'],
                    'product_id'=>null,
                    'product_code'=>$productData['product_code'],
                    'variant' => $variant[$var],
                    'qty'=>$value['qty'],
                    'image_gallery'=>$value['image_gallery'],
                    'status'=>$value['status']
                ];
                unset($variant[$var]);
            }

            if (!empty($variant)) {
                // $thisViewModel->sendError("all product variant must be provided", 400);
            }
        }
        $productSKU = array_values($productSKU);
        $productData['product_sku'] = $productSKU;

        $returnField = "product_name,product_code,description,tnc,variation,type,category,status,weight,dimensions,volume_weight,images_gallery,product_sku";
        $returnField = explode(",",$returnField);
        
        $returnResult = [];
        foreach ($returnField as $key => $value) {
            if (isset($productData[$value])) {
                $returnResult[$value] = $productData[$value];
            }
        }

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
