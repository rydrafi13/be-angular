<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

function calculateVolumetricWeight($arguments, $thisViewModel) {
    $length = $arguments[0];
    $width = $arguments[1];
    $height = $arguments[2];

    try {

        // dimensions in cm
        ( empty($length) || (!preg_match("/^[0-9]+([.][0-9]+)?$/i", strval($length)) )) ?
            $thisViewModel->sendError("dimension length value {$length} is invalid", 400):
            $length = doubleval($length);

        ( empty($width) || (!preg_match("/^[0-9]+([.][0-9]+)?$/i", strval($width))) ) ?
            $thisViewModel->sendError("dimension width value {$width} is invalid", 400):
            $width = doubleval($width);

        ( empty($height) || (!preg_match("/^[0-9]+([.][0-9]+)?$/i", strval($height))) ) ?
            $thisViewModel->sendError("dimension height value {$height} is invalid", 400):
            $height = doubleval($height);

        // calculate in grams
        $volumetricWeight = doubleval((($length * $width * $height) / 6000) * 1000);

        $returnResult = [
            'dimensions'=>[
                'length' => $length,
                'width' => $width,
                'height' => $height,
            ],
            'volume_weight' => $volumetricWeight
        ];
        return $returnResult;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}