<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductSkuModel;
use App\RetailerProgram\Models\EvoucherModel;

function stockUpdateOnOrderInvoice($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $orderData = $arguments[1];
    $orderId = $arguments[2];

    try {

        $ProductSkuModel = new ProductSkuModel();
        $ProductSkuModel->setCurrentUser($auth['user_id']);

        $EvoucherModel = new EvoucherModel;
        $EvoucherModel->setCurrentUser($auth['user_id']);

        $productData = $orderData['products'];

        $updateProduct = [];
        $updateEvoucher = [];
        $ownerID = $EvoucherModel->convertToObjectId($orderData['user_id']);
        foreach ($productData as $key => $value) {
            switch ($value['type']) {
                case 'product':
                case 'voucher':
                    $updateProduct[] = [
                        'filter'=>[
                            '_id'=>$ProductSkuModel->convertToObjectId($value['product_id'])
                        ],
                        'new_value'=>[
                            'qty'=>intval($value['qty']) - intval($value['quantity'])
                        ]
                    ];
                    break;
                
                case 'e-voucher':
                    $getEvoucher = $EvoucherModel->getAvailableStock($value['product_code'], $value['sku_code'], $value['quantity']);
                    $getEvoucher = $thisViewModel->objectToArray($getEvoucher['result']);

                    foreach ($getEvoucher as $skey => $svalue) {
                        $updateEvoucher[] = [
                            'filter'=>['_id'=>$EvoucherModel->convertToObjectId($svalue['_id'])],
                            'new_value'=>[
                                'qty_out'=>1,
                                'status'=>'PENDING',
                                'owner_id'=>$ownerID,
                                'reference_no'=>$orderId,
                            ]
                        ];   
                    }
                    break;
            }
        }

        if (!empty($updateProduct))
            $ProductSkuModel->updateBatch($updateProduct);
        if (!empty($updateEvoucher))
            $EvoucherModel->updateBatch($updateEvoucher);
       
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}