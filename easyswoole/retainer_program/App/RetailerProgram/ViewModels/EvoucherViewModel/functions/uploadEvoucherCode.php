<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\ProductModel;
use App\RetailerProgram\Models\ProductSkuModel;
use App\RetailerProgram\Models\EvoucherModel;
use App\RetailerProgram\Models\EvoucherStockModel;
use App\GeneralData\ViewModels\MediaViewModel;
use App\GeneralData\ViewModels\CryptoViewModel;
use Services\SpreadsheetService;

function uploadEvoucherCode($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];

    try {

        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $document, "VOUCHER_UPLOAD");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $data = [
            'user_id' => $auth['user_id'],
            'files'=>$files,
            'process_no'=>$document['process_no']
        ];
        if (in_array($document['ext'], ['xls','xlsx'])) {
            $result = processXlsx($data, $thisViewModel);
        } elseif (in_array($document['ext'], ['csv'])) {
            
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function processXlsx($data, &$thisViewModel) {
    try {

        $requiredField = "product_code,sku_code,voucher_code,expiry_date";

        $ProductModel = new ProductModel();
        $ProductSkuModel = new ProductSkuModel();

        $CryptoViewModel = new CryptoViewModel();

        $EvoucherModel = new EvoucherModel();
        $EvoucherModel->setCurrentUser($data['user_id']);

        $EvoucherStockModel = new EvoucherStockModel();
        $EvoucherStockModel->setCurrentUser($data['user_id']);

        $processNumber = $data['process_no'];
        $voucherCode = [];
        $newVoucher = [];


        $products = [];
        $skus = [];
        $totalValue = 0;
        $totalPrice = 0;
        $expiryDate = [];
        $i = 1;
        foreach ($data['files'] as $key => $value) {

            if (isset($products[$value['product_code']])) {
                $prodId = $products[$value['product_code']];
            } else {

                $findProduct = $ProductModel->findByProductCodeOrId($value['product_code'], 'ACTIVE');
                if (empty($findProduct['result'])) {
                    $thisViewModel->sendError("product code {$value['product_code']} does not exists", 404);
                }
                if ($findProduct['result'][0]->type != "e-voucher") {
                    $thisViewModel->sendError("product must be type of e-voucher", 400);
                }

                $prodId = $EvoucherModel->convertToObjectId($findProduct['result'][0]->_id);
                $products[$value['product_code']] = $prodId;
            }

            if (isset($skus[$value['sku_code']])) {
                $skuId = $skus[$value['sku_code']]['id'];
                $skuValue = $skus[$value['sku_code']]['value'];
                $skuPrice = $skus[$value['sku_code']]['price'];
            } else {
                $filter = [
                    'product_code'=>$value['product_code'],
                    'sku_code'=>$value['sku_code'],
                    'status'=>'ACTIVE'
                ];
                $findSku = $ProductSkuModel->findProductSku($filter);
                if (empty($findSku['result'])) {
                    $thisViewModel->sendError("sku code {$value['sku_code']} does not exists", 404);
                }

                $skuId = $EvoucherModel->convertToObjectId($findSku['result'][0]->_id);
                $skuValue = $findSku['result'][0]->sku_value;
                $skuPrice = $findSku['result'][0]->sku_price;
                $skus[$value['sku_code']] = [
                    'id'=>$skuId,
                    'value'=>$skuValue,
                    'price'=>$skuPrice
                ];
            }
            $totalValue += $skuValue;
            $totalPrice += $skuPrice;

            if (!preg_match("/^([0-9]{3}|[1-2][0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/i", $value['expiry_date'])) {
                $thisViewModel->sendError("Invalid date format {$value['expiry_date']}, only \"YYYY-MM-DD\" format allowed", 400);
            }

            if (in_array($value['voucher_code'], $voucherCode)) {
                $thisViewModel->sendError("duplicate voucher code {$value['voucher_code']}", 409);
            }
            $voucherCode[] = $value['voucher_code'];

            if (isset($expiryDate[$value['expiry_date']])) {
                $expDate = $expiryDate[$value['expiry_date']];
            } else {
                $date = createDateTime($value['expiry_date'], 'format', "Y-m-d");
                if ($date <= date("Y-m-d")) {
                    $thisViewModel->sendError("invalid expiry date value", 400);
                }
                $expDate = $EvoucherModel->convertToMongoDateTime($date);
                $expiryDate[$value['expiry_date']] = $expDate;
            }

            // $secret = $CryptoViewModel->aesEncrypt($value['voucher_code'], $thisViewModel->hashKey);
            $secret = $CryptoViewModel->sslEncrypt($value['voucher_code']);
            $findVoucher = $EvoucherModel->findEvoucher(['voucher_code'=>$secret]);
            if (!empty($findVoucher['result'])) {
                $thisViewModel->sendError("voucher code {$value['voucher_code']} already exists", 409);
            }
            
            $newVoucher[] = [
                'product_id'=>$prodId,
                'product_code'=>$value['product_code'],
                'sku_id'=>$skuId,
                'sku_code'=> $value['sku_code'],
                'voucher_code'=>$secret,
                'expiry_date'=>$expDate,
                'process_number'=>$processNumber,
                'process_id'=>$processNumber."-".sprintf("%03d", $i++),
                'qty_in'=>0,
                'qty_out'=>0,
                'status'=>'INACTIVE',
            ];
        }

        if (!empty($newVoucher)){
            $processEvoucher = [
                'process_number'=>$processNumber,
                'process_type'=>'UPLOAD',
                'status'=>'REQUESTED',
                'total_sku'=>count($skus),
                'total_qty'=>count($newVoucher),
                'total_value'=>$totalValue,
                'total_price'=>$totalPrice,
                'request_date'=>$EvoucherStockModel->convertToMongoDateTime(date('Y-m-d H:i:s')),
                'request_at'=>getClientIpAddr(),
                'request_by'=>$EvoucherStockModel->convertToObjectId($data['user_id'])
            ];
            $evoucherRequest = $EvoucherStockModel->insert($processEvoucher);

            $result = $EvoucherModel->insertBatch($newVoucher);
        } else 
            $result = ['result'=>['status'=>'No Voucher Code Uploaded']];

        return $result;
    }catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}