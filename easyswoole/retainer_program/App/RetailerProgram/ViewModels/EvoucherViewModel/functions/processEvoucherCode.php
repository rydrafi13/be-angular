<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherModel;
use App\RetailerProgram\Models\EvoucherStockModel;

function processEvoucherCode($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {

        $EvoucherModel = new EvoucherModel();
        $EvoucherModel->setCurrentUser($auth['user_id']);
        $EvoucherStockModel = new EvoucherStockModel();
        $EvoucherStockModel->setCurrentUser($auth['user_id']);

        $filter = [
            'process_number'=>$bodyData['process_number'],
            'status'=>'REQUESTED'
        ];
        $evStock = $EvoucherStockModel->findStockDetail($filter);
        if (empty($evStock['result'])) {
            $thisViewModel->sendError("process number does not exists", 404);
        }

        $ids = [];
        $processNumber = [];
        foreach ($evStock['result'] as $key => $value) {
            $ids[] = $EvoucherStockModel->convertToObjectId($value->_id);
            $processNumber[] = $value->process_number;
        }

        if ($bodyData['status'] == 'approve') {
            $approveProcess = $EvoucherStockModel->update(
                ['_id'=>['$in'=>$ids]], 
                [
                    'status'=>'APPROVED',
                    'approve_date'=>$EvoucherStockModel->convertToMongoDateTime(date('Y-m-d H:i:s')),
                    'approve_at'=>getClientIpAddr(),
                    'approve_by'=>$EvoucherStockModel->convertToObjectId($auth['user_id'])
                ]
            );

            $filter = [
                'process_number'=>['$in'=>$processNumber],
                'qty_in'=>0,
                'qty_out'=>0,
                'status'=>'INACTIVE'
            ];
            $update = $EvoucherModel->update($filter, ['qty_in'=>1, 'status'=>'ACTIVE']);
        } elseif ($bodyData['status'] == 'cancel') {
            $cancelProcess = $EvoucherStockModel->update(
                ['_id'=>['$in'=>$ids]], 
                [
                    'status'=>'CANCELED',
                    'cancel_date'=>$EvoucherStockModel->convertToMongoDateTime(date('Y-m-d H:i:s')),
                    'cancel_at'=>getClientIpAddr(),
                    'cancel_by'=>$EvoucherStockModel->convertToObjectId($auth['user_id'])
                ]
            );

            $filter = [
                'process_number'=>['$in'=>$processNumber],
                'qty_in'=>0,
                'qty_out'=>0,
                'status'=>'INACTIVE'
            ];
            $update = $EvoucherModel->update(
                $filter,
                [
                    'deleted'=>1,
                    'deleted_date'=>$EvoucherModel->convertToMongoDateTime(date('Y-m-d H:i:s')),
                    'deleted_at'=>getClientIpAddr(),
                    'deleted_by'=>$EvoucherModel->convertToObjectId($auth['user_id'])
                ]
            );
        } else {
            $thisViewModel->sendError("invalid operation", 400);
        }

        $result = ['result'=>'success'];
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}