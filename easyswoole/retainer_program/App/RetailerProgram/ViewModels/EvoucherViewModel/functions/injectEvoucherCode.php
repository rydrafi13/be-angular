<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\MemberModel;
use App\RetailerProgram\Models\EvoucherModel;
use App\RetailerProgram\Models\ProductSkuModel;
use App\GeneralData\ViewModels\MediaViewModel;
use App\GeneralData\ViewModels\CryptoViewModel;
use Services\SpreadsheetService;

function injectEvoucherCode($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];

    try {

        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $document, "VOUCHER_UPLOAD");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $CryptoViewModel = new CryptoViewModel();

        $MemberModel = new MemberModel();
        $ProductSkuModel = new ProductSkuModel();
        $EvoucherModel = new EvoucherModel();
        $EvoucherModel->setCurrentUser($auth['user_id']);

        $member = [];
        $product = [];
        $evoucher = [];
        foreach ($files as $key => $value) {
            $username = trim(strval($value['username']));
            $expiryDate = trim($value['expiry_date']);
            $productCode = trim($value['product_code']);
            $evoucherCode = trim($value['voucher_code']);
            
            if (empty($member[$username])) {
                $getMember = $MemberModel->findByUsernameOrId($username);
                empty($getMember['result']) ? 
                    $thisViewModel->sendError("member {$username} does not exists", 400) : 
                    $getMember = $thisViewModel->objectToArray($getMember['result'][0]);
                $getMember['_id'] = $MemberModel->convertToObjectId($getMember['_id']);
                $member[$username] = $getMember;
            }

            if (empty($product[$productCode])) {
                $getProduct = $ProductSkuModel->findProductSku(['product_code'=>$productCode]);
                empty($getProduct['result']) ?
                    $thisViewModel->sendError("product {$productCode} does not exists", 400): 
                    $getProduct = $thisViewModel->objectToArray($getProduct['result'][0]);

                $getProduct['product_id'] = $ProductSkuModel->convertToObjectId($getProduct['product_id']);
                $getProduct['sku_id'] = $ProductSkuModel->convertToObjectId($getProduct['_id']);

                $product[$productCode] = $getProduct;
            }

            $label = $thisViewModel->generateEvoucherLabel($auth, $evoucherCode);
            $evoucher[] = [
                'product_id'=>$product[$productCode]['product_id'],
                'product_code'=>$product[$productCode]['product_code'],
                'sku_id'=>$product[$productCode]['sku_id'],
                'sku_code'=>$product[$productCode]['sku_code'],
                'voucher_code'=>$CryptoViewModel->sslEncrypt($evoucherCode),
                'expiry_date'=>$EvoucherModel->convertToMongoDateTime($expiryDate),
                'process_number'=>trim($value['process_number']),
                'process_id'=>trim($value['process_id']),
                'qty_in'=>1,
                'qty_out'=>1,
                'status'=>'RESERVED',
                'owner_id'=>$member[$username]['_id'],
                'reference_no'=>trim($value['reference_no']),
                'barcode'=>$CryptoViewModel->sslEncrypt($label['barcode']),
                'qr_code'=>$CryptoViewModel->sslEncrypt($label['qr_code'])
            ];
        }

        !empty($evoucher) ?
            $result = $EvoucherModel->insertBatch($evoucher) : null;

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}