<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherStockModel;

function getStockDetail($arguments, $thisViewModel) {
    $processNumber = $arguments[0];

    try {

        $EvoucherStockModel = new EvoucherStockModel();

        $result = $EvoucherStockModel->findStockDetail(['process_number'=>$processNumber]);
        if (empty($result['result'])) {
            $thisViewModel->sendError("invalid process number", 400);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}