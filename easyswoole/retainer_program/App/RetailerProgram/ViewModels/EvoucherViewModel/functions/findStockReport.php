<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherStockModel;
use App\GeneralData\ViewModels\ReportViewModel;

function findStockReport($arguments, $thisViewModel) {
    $search = $arguments[0];
    $order_by = $arguments[1];
    $page = $arguments[2];
    $limit_per_page = $arguments[3];
    $requested_columns = $arguments[4];
    $download = $arguments[5];

    try {

        $EvoucherStockModel = new EvoucherStockModel();

        if (empty($requested_columns)) {
            $requested_columns = 'process_number,process_type,status,total_sku,total_qty,total_value,total_price,request_date,request_at,request_by,approve_Date,approve_at,approve_by,cancel_date,cancel_at,cancel_by';
        }
        $requested_columns = explode(",", $requested_columns);

        if ($download) {
            $result_db = $EvoucherStockModel->findAllReport($search, $order_by, $page, null, $requested_columns);
            $result_db = json_decode(json_encode($result_db['result']['values']), 1);

            if (count($result_db) == 0) {
                $thisViewModel->sendResult('Empty result founded, No need to convert it as Excel / Csv Spread sheet', 400);
            } else {
                $ReportViewModel = new ReportViewModel();

                $result = $ReportViewModel->generateReport($result_db, "EVOUCHER_STOCK");
            }
            
        } else {
            $result = $EvoucherStockModel->findAllReport($search, $order_by, $page, $limit_per_page, $requested_columns);
            $result['result']['values'] = json_decode(json_encode($result['result']['values']), 1);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}