<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function reserveEvoucher($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $ownerId = $arguments[1];
    $referenceNo = $arguments[2];

    try {
        $CryptoViewModel = new CryptoViewModel();

        $EvoucherModel = new EvoucherModel();
        $EvoucherModel->setCurrentUser($auth['user_id']);

        $ownerId = $EvoucherModel->convertToObjectId($ownerId);
        $filter = [
            'status'=>'PENDING',
            'owner_id'=>$ownerId,
            'reference_no'=>$referenceNo
        ];
        $getEvoucher = $EvoucherModel->findEvoucher($filter);

        if (!empty($getEvoucher['result'])) {
            $updateBatch = [];
            foreach ($getEvoucher['result'] as $key => $value) {
                $updateData = [
                    'status'=>'RESERVED'
                ];
                if (empty($value->barcode || empty($value->qr_code))) {
                    $evoucherCode = $CryptoViewModel->sslDecrypt($value->voucher_code);
                    $label = $thisViewModel->generateEvoucherLabel($auth, $evoucherCode);
                    $updateData['barcode'] = $CryptoViewModel->sslEncrypt($label['barcode']);
                    $updateData['qr_code'] = $CryptoViewModel->sslEncrypt($label['qr_code']);
                }
                $updateBatch[] = [
                    'filter' => ['_id'=>$EvoucherModel->convertToObjectId($value->_id)],
                    'new_value' => $updateData
                ];
            }
            $result = $EvoucherModel->updateBatch($updateBatch);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}