<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function redeemEvoucher($arguments, $thisViewModel) {
    $processId = $arguments[0];
    $auth = $arguments[1];

    try {

        $EvoucherModel = new EvoucherModel();
        $CryptoViewModel = new CryptoViewModel();

        $filter = [
            'process_id'=>$processId,
            'qty_in'=>1,
            'qty_out'=>1,
            'status'=>'RESERVED',
            'expiry_date'=>['from'=>date('Y-m-d')]
        ];
        
        ($auth['type'] == 'member') ? $filter['owner_id'] = $EvoucherModel->convertToObjectId($auth['subject_id']) : null;
        $result = $EvoucherModel->findEvoucher($filter);
        
        empty($result['result'])?
            $thisViewModel->sendError("e-voucher is invalid", 400) :
            $result = $thisViewModel->objectToArray($result['result']);

        $evoucherIds = [];
        foreach ($result as $key => $value) {
            !empty($value['voucher_code'])?
                $value['voucher_code'] = $CryptoViewModel->sslDecrypt($value['voucher_code']):
                null;

            !empty($value['barcode'])?
                $value['barcode'] = $CryptoViewModel->sslDecrypt($value['barcode']):
                null;

            !empty($value['qr_code'])?
                $value['qr_code'] = $CryptoViewModel->sslDecrypt($value['qr_code']):
                null;

            !empty($value['redeem_url'])?
                $value['redeem_url'] = $CryptoViewModel->sslDecrypt($value['redeem_url']):
                null;

            $evoucherIds[] = $EvoucherModel->convertToObjectId($value['_id']);
            unset($value['_id']);
            $result[$key] = $value;
        }

        $updateStatus = $EvoucherModel->update(
            ['_id'=>['$in'=>$evoucherIds]],
            [
                'status'=>'REDEEM',
                'redeem_at'=>getClientIpAddr(),
                'redeem_date'=>$EvoucherModel->convertToMongoDateTime(date('Y-m-d H:i:s')),
                'redeem_by'=>$EvoucherModel->convertToObjectId($auth['subject_id'])
            ]
        );

        $returnResult = ['result'=>$result];
        return $returnResult;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}