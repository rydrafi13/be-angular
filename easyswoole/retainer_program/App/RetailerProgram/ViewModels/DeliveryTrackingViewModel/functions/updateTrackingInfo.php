<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\GeneralData\ViewModels\CourierViewModel;
use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\UserManagement\ViewModels\AppListViewModel;

function updateTrackingInfo($arguments, $thisViewModel) {
    global $selectedDB;
    try {
        $AppListViewModel = new AppListViewModel();
        $appList = $AppListViewModel->findApp(['status'=>'ACTIVE']);

        if (!empty($appList['result'])) {
            $CourierViewModel = new CourierViewModel();

            foreach ($appList['result'] as $key => $value) {
                $selectedDB = $value->app_name;

                $DeliveryTrackingModel = new DeliveryTrackingModel();
                $delivery = $DeliveryTrackingModel->findOnDelivery();
                if (!empty($delivery['result'])) {
                    $delivery = $thisViewModel->objectToArray($delivery['result']);

                    $updateData = [];
                    foreach ($delivery as $row => $data) {
                        $shippingInfo = $data['shipping_info'];

                        switch ($data['courier']) {
                            case 'REALS-SAP':
                                $bodyData = [
                                    'tracking'=>'awb_no',
                                    'awb_number'=>$data['awb_number']
                                ];
                                $trackingInfo = $CourierViewModel->courierTrackingSAP($bodyData);
                                if(!empty($trackingInfo)){
                                    $setData = ['track_history' => $trackingInfo];
                                    $lastStatus = end($trackingInfo);

                                    $setData = [];

                                    if(!empty($lastStatus) && strpos($lastStatus['rowstate_name'], "POD - DELIVERED") !== false){
                                        $shippingInfo[] = [
                                            'label'=>'delivered',
                                            'title'=>'Delivered',
                                            'receiver_name'=>$lastStatus['receiver_name'],
                                            'remarks'=>$lastStatus['description'],
                                            'created_date'=>date('Y-m-d H:i:s'),
                                            'updated_date'=>date('Y-m-d H:i:s')
                                        ];

                                        foreach ($shippingInfo as $sk => $sval) {
                                            $sval['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($sval['created_date']);
                                            $sval['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($sval['updated_date']);

                                            $shippingInfo[$sk] = $sval;
                                        }

                                        $setData['shipping_info'] = $shippingInfo;
                                        $setData['status'] = 'DELIVERED';
                                    }

                                    $updateData[] = [
                                        'filter'=>['_id'=>$DeliveryTrackingModel->convertToObjectId($data['_id'])],
                                        'new_value'=>$setData
                                    ];
                                }
                                break;
                        }
                    }

                    !empty($updateData) ? 
                        $DeliveryTrackingModel->updateBatch($updateData) : null;
                }
            }
        }

        return $result = [
            'result' => 'success'
        ];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}