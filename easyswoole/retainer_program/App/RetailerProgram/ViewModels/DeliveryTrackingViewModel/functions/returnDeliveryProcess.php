<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\RetailerProgram\Models\OrderInvoiceModel;
use App\RetailerProgram\ViewModels\PointsTransactionViewModel;
use App\RetailerProgram\ViewModels\ProductViewModel;

function returnDeliveryProcess ($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {

        switch ($method) {
            case 'single':
                $DeliveryTrackingModel = new DeliveryTrackingModel();
                $DeliveryTrackingModel->setCurrentUser($auth['user_id']);
                $deliveryTracking = $DeliveryTrackingModel->findByRefno($bodyData['ref_no'], ['status'=>['DELIVERED','RETURN']]);
                
                empty($deliveryTracking['result']) ?
                $thisViewModel->sendError("Delivery data for reference number {$bodyData['ref_no']} not found", 404) :
                    $deliveryTracking = $thisViewModel->objectToArray($deliveryTracking['result'][0]);
                    
                    if (!preg_match("/^([0-9]{3}|[1-2][0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/i", $bodyData['return_date'])) {
                        $thisViewModel->sendError("Invalid date format {$bodyData['return_date']}, only \"YYYY-MM-DD\" format allowed", 400);
                    }
                    
                    $shippingInfo = $deliveryTracking['shipping_info'] ? :[];
                    $lastShippingInfo = end($shippingInfo);
                    $returnDetail = $deliveryTracking['return_detail'] ? : [];
                    
                    if(!empty($lastShippingInfo) && !in_array($lastShippingInfo['label'], ['delivered','return']))  $thisViewModel->sendError("invalid operation", 400);
                    
                    $returnDate = createDateTime($bodyData['return_date'], 'format', "Y-m-d H:i:s");
                    $date = date("Y-m-d H:i:s");
                    $updatedDate = $date;
                    $returnId = $DeliveryTrackingModel->convertToObjectId(true,true)->__toString();
                    
                    if(!empty($lastShippingInfo) && $lastShippingInfo['label'] == 'return') {
                        $returnId = $lastShippingInfo['id'];
                        $date = $lastShippingInfo['created_date'];
                        $updatedDate = date("Y-m-d H:i:s");
                        array_pop($shippingInfo);
                        
                        $tempReturnDetail = $returnDetail;
                        $returnDetail = array();
                        foreach ($tempReturnDetail as $key => $value){
                            if($value['return_id'] == $returnId){
                                $value['courier'] = $bodyData['courier'];
                                $value['awb_number'] = $bodyData['awb_number'];
                                $value['return_date'] = $returnDate;
                                $value['created_date'] = $date;
                                $value['updated_date'] = $updatedDate;
                            }
                            $returnDetail[] = $value;
                        }                    
                    }
                    
                    else {
                        $returnDetail[] = [
                            'return_id' => $returnId,
                            'courier'=>$bodyData['courier'],
                            'awb_number'=>$bodyData['awb_number'],
                            'return_date'=>$returnDate,
                            'created_date'=>$date,
                            'updated_date'=>$updatedDate
                        ];
                    }
                    
                    $shippingInfo[] = [
                        'id' => $returnId,
                        'label'=>'return',
                        'title'=>'Return Order',
                        'remarks'=>$bodyData['remarks'],
                        'created_date'=>$date,
                        'updated_date'=>$updatedDate
                    ];
                    
                    $OrderInvoiceModel = new OrderInvoiceModel();
                    $orderInvoice = $OrderInvoiceModel->findByOrderId($deliveryTracking['order_id']);

                    empty($orderInvoice['result']) ?
                            $thisViewModel->sendError("Order Invoice related with reference number  {$bodyData['ref_no']} not found", 404) :
                            $orderInvoice = $thisViewModel->objectToArray($orderInvoice['result'][0]);

                    if ($bodyData['return_type'] == 'REORDER') {

                        if(!isset($orderInvoice['points_by'])) $thisViewModel->sendError("reference number  {$bodyData['ref_no']} cannot reorder", 400);
                        
                        if(!empty($lastShippingInfo) && $lastShippingInfo['label'] != 'delivered')  $thisViewModel->sendError("invalid operation", 400);
                        
                        if(empty($bodyData['product_sku']) || !is_array($bodyData['product_sku'])) $thisViewModel->sendError('empty array product_sku is not allowed', 400);
                        
                        
                        $cancelDetail = array(
                            'cancel_date'=>$date,
                            'cancel_at'=>getClientIpAddr(),
                            'cancel_by'=>$auth['user_id']
                        );
                        
                        $bodyData['product_sku'] = array_unique($bodyData['product_sku']);

                        $productsRefund = array();

                        foreach($bodyData['product_sku'] as $productSku){

                            $updateProductShipping = array();
                            $updateProductInvoice = array();

                            $foundMatch = false;
                            foreach($deliveryTracking['product_list'] as $productShipping){
                                if($productSku == $productShipping['sku_code']){
                                    $foundMatch = true;
                                    $productShipping['cancel_detail'] = $cancelDetail;
                                    $productsRefund[] = $productShipping;
                                }
                                $updateProductShipping[] = $productShipping;
                            }
                            
                            if($foundMatch == false) $thisViewModel->sendError("product_sku {$productSku} not found ", 400);
                            
                            $foundMatch = false;
                            
                            foreach($orderInvoice['products'] as $productInvoice){
                                if($productSku == $productInvoice['sku_code']){
                                    $foundMatch = true;
                                    $productInvoice['cancel_detail'] = $cancelDetail;
                                }
                                $updateProductInvoice[] = $productInvoice;
                            }

                            if($foundMatch == false) $thisViewModel->sendError("product_sku {$productSku} not found ", 400);
                            
                            $deliveryTracking['product_list'] = $updateProductShipping;
                            $orderInvoice['products'] = $updateProductInvoice;
                        }

                        $PointsTransactionViewModel = new PointsTransactionViewModel();
                        $ProductViewModel = new ProductViewModel();
                        
                        $PointsTransactionViewModel->refundRedeemedPoints($auth, $orderInvoice, $bodyData['product_sku']);
                        
                        $deliveryTracking['products'] = $productsRefund;
                        $deliveryTracking['user_id'] = $orderInvoice['user_id'];
                        $ProductViewModel->stockReturnOnOrderInvoice($auth, $deliveryTracking);
                        unset($deliveryTracking['products']);  
                        unset($deliveryTracking['user_id']);

                        $setData = [
                            'status'=>'REORDER',
                            'product_list' => $deliveryTracking['product_list'],
                            'cancel_date'=>$date,
                            'cancel_at'=>getClientIpAddr(),
                            'cancel_by'=>$DeliveryTrackingModel->convertToObjectId($auth['user_id']),
                            'shipping_info' => $shippingInfo,
                            'return_detail' => $returnDetail,
                        ];
                        
                        
                }
                
                else if ($bodyData['return_type'] == 'REDELIVER') {
                    
                    $setData = [
                        'status'=>'RETURN',
                        'shipping_info' => $shippingInfo,
                        'return_detail' => $returnDetail,
                    ];
                }
                
                foreach ($setData['shipping_info'] as $key => $value) {
                    empty($value['id']) ? :$value['id'] = $DeliveryTrackingModel->convertToObjectId($value['id']);
                    $value['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['created_date']);
                    $value['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['updated_date']);
                    
                    $setData['shipping_info'][$key] = $value;
                }
                $setData['shipping_info'] = array_values($setData['shipping_info']);
                
                foreach ($setData['return_detail'] as $key => $value) {
                    $value['return_id'] = $DeliveryTrackingModel->convertToObjectId($value['return_id']);
                    $value['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['created_date']);
                    $value['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['updated_date']);
                    $value['return_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['return_date']);
                    
                    $setData['return_detail'][$key] = $value;
                }
                $setData['return_detail'] = array_values($setData['return_detail']);
                
                $updateData = [
                    'products' => $orderInvoice['products']
                ];

                $OrderInvoiceModel->updateByID($orderInvoice['_id'], $updateData);
                
                $result = $DeliveryTrackingModel->updateByID($deliveryTracking['_id'], $setData);
                
                break;
                
                case 'bulk':
                    # code...
                    $thisViewModel->sendError("method bulk not available", 404);
                    break;
                    
                    default :
                    $thisViewModel->sendError("methods parameter is required", 404);
                    break;
                }
                
                
                return $result;
                
            } catch (\Exception $e) {
                $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
            } catch (\Error $e) {
                $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
            }
        }