<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;

function markLabelPrinted($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {

        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($auth['user_id']);

        $updateData = [];
        foreach ($bodyData['reference_no'] as $key => $value) {
            $filter = ['status'=>'ACTIVE'];
            $delivery = $DeliveryTrackingModel->findByRefno($value, $filter);

            empty($delivery['result'])?
                $thisViewModel->sendError("Delivery data not found", 404):
                $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

            $lastShippingInfo = end($delivery['shipping_info']);

            if ($lastShippingInfo['label'] != 'on_delivery' && $lastShippingInfo['label'] != 'on_processing') {
                $thisViewModel->sendError("invalid shipping label", 400);
            }

            $delivery['additional_info']['label_printed'] = $bodyData['status'];

            $updateData[] = [
                'filter'=>['_id'=>$DeliveryTrackingModel->convertToObjectId($delivery['_id'])],
                'new_value'=>[
                    'additional_info'=>$delivery['additional_info']
                ]
            ];
        }

        !empty($updateData) ?
            $result = $DeliveryTrackingModel->updateBatch($updateData): null;

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}