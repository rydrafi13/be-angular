<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;

function updateAdditionalInfo($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {
        
        $acceptedField = explode(",", "bast,surat_kuasa,surat_pernyataan");

        !is_array($bodyData) ?
            $thisViewModel->sendError("invalid payload", 400): null;
        empty($bodyData['username']) ? 
            $thisViewModel->sendError("username is required", 400): null;

        $dataFromClient = [];
        foreach ($acceptedField as $key => $value) {
            if (!empty($bodyData[$value]) && is_array($bodyData[$value])) {
                $dataFromClient[$value] = $bodyData[$value];
            } 
        }

        empty($dataFromClient) ?
            $thisViewModel->sendError("invalid payload", 400) : null;

        
        $MemberModel = new MemberModel();
        $member = $MemberModel->findByUsernameOrId($bodyData['username']);
        empty($member['result'])?
            $thisViewModel->sendError("username {$bodyData['username']} does not exists", 400):
            $dataFromServer = $thisViewModel->objectToArray($member['result'][0]->additional_info);

        switch ($method) {
            case 'add':
                foreach ($dataFromClient as $key => $value) {
                    $clientData = [];
                    foreach ($value as $k => $val) {
                        $clientData[$k] = [
                            'id'=>$MemberModel->convertToObjectId(null, true)->__toString(),
                            'pic_file_name'=>$val,
                            'created_date'=>date('Y-m-d H:i:s'),
                            'created_by'=>$auth['user_id'],
                            'updated_date'=>date('Y-m-d H:i:s'),
                            'updated_by'=>$auth['user_id']
                        ];
                    }
                    
                    empty($dataFromServer[$key]) ?
                        $dataFromServer[$key] = $clientData:
                        $dataFromServer[$key] = array_merge($dataFromServer[$key], $clientData);
                }
                $result = $MemberModel->updateByID($member['result'][0]->_id, ['additional_info' => $dataFromServer]);
                break;
            
            case 'remove':
                foreach ($dataFromClient as $key => $value) {
                    
                    if (!empty($dataFromServer[$key])) {
                        $dataServer = [];
                        foreach ($dataFromServer[$key] as $k => $val) {
                            if (!in_array($val['id'], $value)) {
                                $dataServer[] = $val;
                            }
                        }
                        $dataFromServer[$key] = $dataServer;
                    }
                }

                $result = $MemberModel->updateByID($member['result'][0]->_id, ['additional_info' => $dataFromServer]);
                break;
        }
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}