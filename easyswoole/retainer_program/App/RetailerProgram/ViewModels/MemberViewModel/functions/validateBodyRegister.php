<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

function validateBodyRegister($arguments, $thisViewModel) {
    $bodyData = $arguments[0];
    try {

        $emptyFields = [];

        empty($bodyData['full_name']) ?
            $emptyFields[] = 'full_name' : null;

        empty($bodyData['username']) ?
            $emptyFields[] = 'username' : null;

        empty($bodyData['cell_phone']) ?
            $emptyFields[] = 'cell_phone' : null;

        if (!empty($emptyFields)) {
            $emptyFields = implode(", ", $emptyFields);
            $thisViewModel->sendError("field ".$emptyFields." is required", 400);
        }

        if(!empty($bodyData['gender']) && (strtolower($bodyData['gender']) != 'male' && strtolower($bodyData['gender']) != 'female' && strtolower($bodyData['gender']) != 'undefined'))
        {
            $thisViewModel->sendError('Please select your gender', 400);
        } elseif (empty($bodyData['gender'])) {
            $bodyData['gender'] = "undefined";
        }

        if(!empty($bodyData['dob']) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $bodyData['dob']))
        {
            $thisViewModel->sendError('Invalid Format DOB',400);
        } elseif (empty($bodyData['dob'])) {
            $bodyData['dob'] = "0000-00-00";
        }

        $validBody = [
            'full_name'=>$bodyData['full_name'],
            'username'=>$bodyData['username'],
            'cell_phone'=>$bodyData['cell_phone'],
            "email"=>$bodyData['email'],
            'password'=>generateAlphaNumericString(6, 3, 2),
            'dob'=>$bodyData['dob'],
            'gender'=>$bodyData['gender']
        ];

        return $validBody;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}