<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;
use App\GeneralData\ViewModels\ReportViewModel;

function findAllReport($arguments, $thisViewModel) {
    $search = $arguments[0];
    $order_by = $arguments[1];
    $page = $arguments[2];
    $limit_per_page = $arguments[3];
    $requested_columns = $arguments[4];
    $download = $arguments[5];

    try {

        $MemberModel = new MemberModel();
        // utk urutan output
        if (!isset($requested_columns)) {
            $requested_columns = "full_name,username,cell_phone,dob,gender,member_address,status,input_form_data,created_date,updated_date,point_balance,additional_info";
        }
        $requested_columns = explode(",", $requested_columns);

        if ($download) {
            $result_db = $MemberModel->findAllReport($search, $order_by, $page, null, $requested_columns);
            $result_db = json_decode(json_encode($result_db['result']['values']), 1);

            if (count($result_db) == 0) {
                $thisViewModel->sendResult('Empty result founded, No need to convert it as Excel / Csv Spread sheet', 400);
            } else {
                $ReportViewModel = new ReportViewModel();

                $result = $ReportViewModel->generateReport($result_db, "MEMBER_REPORT");
            }
            
        } else {
            $result = $MemberModel->findAllReport($search, $order_by, $page, $limit_per_page, $requested_columns);
            $result['result']['values'] = json_decode(json_encode($result['result']['values']), 1);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}