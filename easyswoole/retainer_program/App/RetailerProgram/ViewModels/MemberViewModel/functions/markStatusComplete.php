<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;

function markStatusComplete($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];
    try {

        $MemberModel = new MemberModel();
        $MemberModel->setCurrentUser($auth['user_id']);

        $memberIds = $bodyData['member_id'];
        $status = $bodyData['status'];
        $updateData = [];
        foreach ($memberIds as $key => $value) {
            $member = $MemberModel->findByUsernameOrId($value);
            empty($member['result'])?
                $thisViewModel->sendError("member id {$value} does not exists", 404):
                $member = $thisViewModel->objectToArray($member['result'][0]);

            $member['additional_info']['data_complete'] = $status;
            $updateData[] = [
                'filter'=>['_id'=>$MemberModel->convertToObjectId($member['_id'])],
                'new_value'=>[
                    'additional_info'=>$member['additional_info']
                ]
            ];
        }

        !empty($updateData) ?
            $result = $MemberModel->updateBatch($updateData): null;
        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}