<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function validateMember($arguments, $thisViewModel) {
    $username = $arguments[0];
    $password = $arguments[1];

    try {

        $MemberModel = new MemberModel();

        $result = $MemberModel->findByUsernameOrId($username, 'ACTIVE');
        if (empty($result['result'])) {
            $thisViewModel->sendError("username is not valid", 400);
        }

        $hash = $result['result'][0]->password;

        $CryptoViewModel = new CryptoViewModel();
        $verify = $CryptoViewModel->verifyPassword($password, $hash);
        if (!$verify) {
            $thisViewModel->sendError("password is not valid", 400);
        }

        return $thisViewModel->objectToArray($result['result'][0]);

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}