<?php
namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\Models\ShoppingcartModel;

function findAllComplete($arguments, $thisViewModel)
{
    try {
        $ShoppingcartModel = new ShoppingcartModel();
        $result = $ShoppingcartModel->findAllComplete($arguments[0], $arguments[1]);
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
