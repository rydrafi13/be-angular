<?php
namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\Models\ShoppingcartModel;

function findActiveCart($arguments, $thisViewModel)
{
    $userID = $arguments[0];
    $createIfEmpty = isset($arguments[1])? $arguments[1] : true;
    try {
        $ShoppingcartModel = new ShoppingcartModel();
        
        $ShoppingcartModel->setCurrentUser($userID);

        /** try to find the user shopping cart */ 
        $result = $ShoppingcartModel->findActiveCartByUserID($userID);        
        
        /** If we can't find the Active Cart, please create new */
        if(empty($result['result']) && $createIfEmpty){
            $ShoppingcartModel->createNewShoppingCartByUserID($userID);
            /** Lets find it again */
            $result = $ShoppingcartModel->findActiveCartByUserID($userID);
        }

        $returnResult = ['result' => $thisViewModel->objectToArray($result['result'][0])];
        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
