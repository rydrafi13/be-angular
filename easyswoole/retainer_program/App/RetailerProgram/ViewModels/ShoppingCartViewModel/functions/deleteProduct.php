<?php
namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\Models\ShoppingcartModel;

function deleteProduct($arguments, $thisViewModel){

    $auth = $arguments[0];
    $getBody = $arguments[1];

    try {

        $ShoppingcartModel = new ShoppingcartModel();
        $ShoppingcartModel->setCurrentUser($auth['user_id']);

        $productsBody = $thisViewModel->validateProducts($getBody['products']);

        $activeSC = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);
        if (empty($activeSC['result'])) {
            $createNewSC = $ShoppingcartModel->createNewShoppingCartByUserID($auth['subject_id']);
            $activeSC = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);
        }
        $activeSC = $thisViewModel->objectToArray($activeSC['result'][0]);

        $productListFromServer=[];
        
        if(!empty($activeSC['products'])){
            foreach ($activeSC['products'] as $productServer) {

                if (!isset($productListFromServer[$productServer['product_id']])) {
                    $productListFromServer[$productServer['product_id']] = $productServer ;
                }
            }
        }
        
        foreach ($productsBody['products'] as $newProduct) {
            if (isset($productListFromServer[$newProduct['product_id']])) {
                unset($productListFromServer[$newProduct['product_id']]);
            }
        }
        
        $validateProduct = $thisViewModel->validateProducts($productListFromServer);
        
        $return = $ShoppingcartModel->update($activeSC['_id'], $validateProduct);

        $result = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);

        return $result;

    }  catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}