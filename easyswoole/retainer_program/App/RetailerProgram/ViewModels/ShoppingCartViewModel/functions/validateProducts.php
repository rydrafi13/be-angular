<?php
namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\ViewModels\ProductViewModel;

function validateProducts($arguments, $thisViewModel)
{
    $productList = $arguments[0];

    try {
        $allowedField = explode(",","product_name,product_code,description,variation,type,tnc,category,status,weight,volume_weight,dimensions,images_gallery,created_date,updated_date,deleted_date,total_qty,sku_code,sku_price,sku_value,variant,qty,image_gallery,product_info,quantity");
        $ProductViewModel = new ProductViewModel();

        //calculate total
        $total = 0;
        $totalQuantity = 0;
        $productInSC = []; /** <-- we need this because sometime the key of products array is not Int ,
                                        * ex: productList['array'];
                                        * we need $products[0] to save it in databases;.
                                        */
        $outOfStock = 0;
        $insufficient = 0;

        // $type = 'voucher';
        foreach ($productList as $row => $product) {
            if ($product['quantity'] < 1) {
                continue;
            }
            foreach ($product as $key => $value) {
                if (!in_array($key, $allowedField)) unset($product[$key]);
            }

            $findProduct = $ProductViewModel->getProductDetail($product['product_code'], 'ACTIVE');
            $findProduct = $thisViewModel->objectToArray($findProduct['result'][0]);

            $productSku = $ProductViewModel->getSkuDetail($product['product_code'], $product['sku_code'], 'ACTIVE');
            $productSku = $thisViewModel->objectToArray($productSku['result'][0]);


            $product = array_merge($product, $findProduct, $productSku);
            if ($productSku['qty'] <= 0) {
                $product['qty'] = 0;
                $product['status'] = 'OS';
                $outOfStock = 1;
                
            } else if ($product['quantity'] > $productSku['qty']) {
                $product['status'] = 'IS';
                $insufficient = 1;
            }
            $product['product_id'] = $product['_id'];
            unset($product['_id']);

            $product['total_product_price'] = $productSku['sku_value'] * $product['quantity'];
            
            $total += $product['total_product_price'] - $productInSC[$product['sku_code']]['total_product_price']?:0;
            $totalQuantity += $product['quantity'] - $productInSC[$product['sku_code']]['quantity']?:0;

            $productInSC[$product['sku_code']] = $product;
        }

        $productInSC = array_values($productInSC);
        $returnResult = [
            'products' => $productInSC,
            'total_price' => $total,
            'total_quantity' => $totalQuantity,
            'product_os' => $outOfStock,
            'product_is' => $insufficient,
            'billings'=>[
                'total_price'=>$total,
                'sum_total'=>$total
            ],
            // 'type'=>$type
        ];

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
