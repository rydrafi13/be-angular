<?php
namespace App\RetailerProgram\ViewModels\PointsTransactionViewModel;

use App\RetailerProgram\Models\PointsTransactionModel;
use App\RetailerProgram\Models\PointsInventoryModel;
use App\RetailerProgram\Models\MemberModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function newPointsTransaction($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];
    $processType = strtoupper($arguments[2]?:'');

    try {

        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $document, "POINTS_TRANSACTION");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $data = [
            'user_id' => $auth['user_id'],
            'files'=>array_values($files),
            'process_no'=>$document['process_no'],
            'process_type'=>$processType
        ];
        if (in_array($document['ext'], ['xls','xlsx'])) {
            switch ($processType) {
                case 'ADD':
                    $result = pointsTransactionAddXLS($data, $thisViewModel);
                    break;
                
                case 'ADJ_IN':
                case 'ADJ_OUT':
                    $result = pointsTransactionAdjXLS($data, $thisViewModel);
                    break;

                default:
                    $thisViewModel->sendError("invalid process type", 400);
                    break;
            }
        } elseif (in_array($document['ext'], ['csv'])) {
            // process csv here
        }

        // $ReportViewModel = new ReportViewModel();
        // $result = $ReportViewModel->generateReport($result, "POINTS_TRANSACTION");

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function pointsTransactionAddXLS($data, &$thisViewModel) {
    try {

        $PointsTransactionModel = new PointsTransactionModel();
        $PointsTransactionModel->setCurrentUser($data['user_id']);
        $MemberModel = new MemberModel();

        $processNumber = $data['process_no'];
        $i = 1;
        $ptsTxData = [];
        $members = [];
        foreach ($data['files'] as $key => $value) {
            $value['points'] = intval($value['points']);
            // if (in_array($value['member_id'], $members)) {
            //     $thisViewModel->sendError("multiple transaction of member id {$value['member_id']}", 400);
            // }

            $findMember = $MemberModel->findByUsernameOrId(strval($value['member_id']));
            if (empty($findMember['result']))
                $thisViewModel->sendError("member {$value['member_id']} does not exists", 404);

            if (isset($members[$value['member_id']])) {
                $userId = $members[$value['member_id']];
            } else {
                $userId = $MemberModel->convertToObjectId($findMember['result'][0]->_id);
                $members[$value['member_id']] = $userId;
            }
            $itemCode = hexdec(uniqid());

            $ptsTxData[] = [
                'points' => $value['points'],
                'user_id'=> $userId,
                'description'=>$value['description'],
                'remarks'=>$value['remarks'],
                'status'=>'REQUESTED',
                'transaction_month'=>date('Y-m'),
                'item_code'=>$itemCode.rand(0,99).sprintf("%03d", $i++),
                'process_type'=>'ADD',
                'process_number'=>$processNumber,
                'process_id'=>$processNumber.'-'.sprintf("%03d", $i++),
            ];
        }

        if (!empty($ptsTxData)) {
            $result = $PointsTransactionModel->insertBatch($ptsTxData);
            $result = ['result'=>['process_number'=>$processNumber]];
        } else {
            $result = ['result'=>['status'=>'no points transaction']];
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function pointsTransactionAdjXLS($data, &$thisViewModel) {
    try {

        $PointsInventoryModel = new PointsInventoryModel();
        $PointsTransactionModel = new PointsTransactionModel();
        $PointsTransactionModel->setCurrentUser($data['user_id']);
        $MemberModel = new MemberModel();

        $processNumber = $data['process_no'];
        $i = 1;
        $ptsTxData = [];
        $members = [];
        foreach ($data['files'] as $key => $value) {
            $value['points'] = intval($value['points']);

            if (empty($value['transaction_month'])) {
                $k = $key + 1;
                $thisViewModel->sendError("field transaction_month on row {$k} must not be empty", 400);
            }

            if (!preg_match("/^([0-9]{3}|[1-2][0-9]{3})-(0[1-9]|1[0-2])$/i", $value['transaction_month'])) {
                $thisViewModel->sendError("Invalid date format {$value['transaction_month']}, only \"YYYY-MM\" format allowed", 400);
            }

            if (in_array($value['member_id'], $members)) {
                $thisViewModel->sendError("multiple transaction of member id {$value['member_id']}", 400);
            }

            $findMember = $MemberModel->findByUsernameOrId(strval($value['member_id']));
            if (empty($findMember['result']))
                $thisViewModel->sendError("member {$value['member_id']} does not exists", 404);
            
            $userId = $MemberModel->convertToObjectId($findMember['result'][0]->_id);

            $filter = [
                'user_id' => $userId,
                'transaction_month'=>$value['transaction_month'],
                'status'=>'REQUESTED',
            ];  
            $ptsAdj = $PointsTransactionModel->DBfind($filter);
            if (!empty($ptsAdj['result'])) {
                $processId = $ptsAdj['result'][0]->process_id;
                $thisViewModel->sendError("points transaction with process id {$processId} on queue", 400);
            }

            $search = [
                'status'=>'ACTIVE',
                'transaction_month'=>$value['transaction_month'],
                'expiry_date'=>date('Y-m')
            ];
            $ptsItem = $PointsInventoryModel->getPointByUserId($findMember['result'][0]->_id, $search);
            empty($ptsItem['result'])?
                $thisViewModel->sendError("no points transaction on {$value['transaction_month']} for member {$value['member_id']}", 404) :
                $ptsItem = $thisViewModel->objectToArray($ptsItem['result']); 
            
            if ($data['process_type'] == 'ADJ_OUT') {
                foreach ($ptsItem as $k => $val) {
                    if ($value['points'] > $val['points']) {
                        $pts = $val['points'];
                    } else {
                        $pts = $value['points'];
                    }
                    $value['points'] -= $pts;
    
                    $ptsTxData[] = [
                        'points'=>$pts,
                        'user_id'=>$userId,
                        'description'=>$value['description'],
                        'remarks'=>$value['remarks'],
                        'status'=>'REQUESTED',
                        'transaction_month'=>$value['transaction_month'],
                        'item_code'=>$val['item_code'],
                        'process_type'=>$data['process_type'],
                        'process_number'=>$processNumber,
                        'process_id'=>$processNumber.'-'.sprintf("%03d", $i),
                    ];
    
                    if ($value['points'] <= 0) break;
                }

                if ($value['points'] > 0) {
                    $ptsTxData[] = [
                        'points'=>$value['points'],
                        'user_id'=>$userId,
                        'description'=>$value['description'],
                        'remarks'=>$value['remarks'],
                        'status'=>'REQUESTED',
                        'transaction_month'=>$value['transaction_month'],
                        'item_code'=>null,
                        'process_type'=>$data['process_type'],
                        'process_number'=>$processNumber,
                        'process_id'=>$processNumber.'-'.sprintf("%03d", $i),
                    ];
                }
            } else if ($data['process_type'] == 'ADJ_IN') {
                $ptsTxData[] = [
                    'points'=>$value['points'],
                    'user_id'=>$userId,
                    'description'=>$value['description'],
                    'remarks'=>$value['remarks'],
                    'status'=>'REQUESTED',
                    'transaction_month'=>$value['transaction_month'],
                    'item_code'=>$ptsItem[0]['item_code'],
                    'process_type'=>$data['process_type'],
                    'process_number'=>$processNumber,
                    'process_id'=>$processNumber.'-'.sprintf("%03d", $i),
                ];
            }

            $i++;
            $members[] = $value['member_id'];
        }

        if (!empty($ptsTxData)) {
            $result = $PointsTransactionModel->insertBatch($ptsTxData);
            $result = ['result'=>['process_number'=>$processNumber]];
        } else
            $result = ['result'=>['status'=>'no points transaction']];

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}