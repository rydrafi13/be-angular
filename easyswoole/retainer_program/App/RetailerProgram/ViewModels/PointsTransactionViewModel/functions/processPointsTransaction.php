<?php
namespace App\RetailerProgram\ViewModels\PointsTransactionViewModel;

use App\RetailerProgram\Models\PointsTransactionModel;
use App\RetailerProgram\Models\PointsInventoryModel;

function processPointsTransaction($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {
        $PointsInventoryModel = new PointsInventoryModel();
        $PointsInventoryModel->setCurrentUser($auth['user_id']);

        $PointsTransactionModel = new PointsTransactionModel();
        $PointsTransactionModel->setCurrentUser($auth['user_id']);

        $filter = [
            'process_number' => $bodyData['process_number'],
            'status'=>'REQUESTED'
        ];
        $ptsTxData = $PointsTransactionModel->DBfind($filter);
        empty($ptsTxData['result']) ?
            $thisViewModel->sendError("process number {$bodyData['process_number']} is invalid", 400):
            $ptsTxData = $thisViewModel->objectToArray($ptsTxData['result']);

        switch ($bodyData['status']) {
            case 'approve':
                $addPtsInv = [];
                $updatePtsTx = [];
                $updatePtsInv = [];
                foreach ($ptsTxData as $key => $value) {
                    $value['points'] = intval($value['points']);
                    
                    if ($value['process_type'] == 'ADD'){
                        $ptsItem = $PointsInventoryModel->getPointByUserId($value['user_id'], ['item_code'=>$value['item_code']]);
                        !empty($ptsItem['result'])?
                            $thisViewModel->sendError("invalid points transaction", 400): null;
                        
                        $expiryDate = date("Y-m", strtotime($value['created_date'] . " + 1 year"));
                        $addPtsInv[] = [
                            'item_code'=>$value['item_code'],
                            'points'=>abs(intval($value['points'])),
                            'user_id'=>$PointsInventoryModel->convertToObjectId($value['user_id']),
                            'expiry_date'=>$PointsInventoryModel->convertToMongoDateTime($expiryDate),
                            'transaction_month'=>$value['transaction_month'],
                            'status'=>'ACTIVE',
                        ];
                    }
                    if ($value['process_type'] == 'ADJ_IN' || $value['process_type'] == 'ADJ_OUT'){
                        if ($value['process_type'] == 'ADJ_OUT')
                            $value['points'] *= -1;

                        if (!empty($value['item_code'])) {
                            $search = [
                                'item_code'=>$value['item_code'],
                                'expiry_date'=>date('Y-m'),
                                'transaction_month'=>$value['transaction_month'],
                                'status'=>'ACTIVE'
                            ];
                            $ptsItem = $PointsInventoryModel->getPointByUserId($value['user_id'], $search);
                            empty($ptsItem['result']) ?
                                $thisViewModel->sendError("points transaction with process is invalid", 400) : null;
    
                            $updatePtsInv[] = [
                                'filter' => ['_id'=>$PointsTransactionModel->convertToObjectId($ptsItem['result'][0]->_id)],
                                'new_value'=>[
                                    'points'=>$ptsItem['result'][0]->points + $value['points']
                                ]
                            ];
                        } else {
                            $itemCode = hexdec(uniqid()).rand(0,99).'001';
                            $addPtsInv[] = [
                                'item_code'=>$itemCode,
                                'points'=>$value['points'],
                                'user_id'=>$PointsInventoryModel->convertToObjectId($value['user_id']),
                                'expiry_date'=>$PointsInventoryModel->convertToMongoDateTime($expiryDate),
                                'transaction_month'=>$value['transaction_month'],
                                'status'=>'ACTIVE',
                            ];

                            $updatePtsTx[] = [
                                'filter' => ['_id'=>$PointsTransactionModel->convertToObjectId($value['_id'])],
                                'new_value'=>[
                                    'item_code'=>$itemCode,
                                    'status'=>'PROCESSED'
                                ]
                            ];
                        }
                    }
                }

                !empty($addPtsInv) ?
                    $result = $PointsInventoryModel->insertBatch($addPtsInv) : null;

                !empty($updatePtsInv) ?
                    $result = $PointsInventoryModel->updateBatch($updatePtsInv): null;

                !empty($updatePtsTx) ?
                    $result = $PointsTransactionModel->updateBatch($updatePtsTx): null;

                $result = $PointsTransactionModel->update($filter, ['status'=>'PROCESSED']);

                break;
            
            case 'cancel':
                /* $itemCode = [];
                foreach ($ptsTxData as $key => $value) {
                    if ($value['process_type'] == 'ADD')
                        $itemCode[] = $value['item_code'];
                }

                $filter = [
                    'item_code'=>['$in'=>$itemCode],
                    'status'=>'INACTIVE'
                ];
                $removeResult = $PointsInventoryModel->DBdelete($filter); */

                $result = $PointsTransactionModel->update($filter, ['status'=>'CANCELLED']);
                break;

            default:
                $thisViewModel->sendError("invalid operation", 400);
                break;
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}