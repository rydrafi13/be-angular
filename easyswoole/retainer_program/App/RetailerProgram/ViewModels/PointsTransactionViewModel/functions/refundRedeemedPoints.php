<?php
namespace App\RetailerProgram\ViewModels\PointsTransactionViewModel;

use App\RetailerProgram\Models\PointsTransactionModel;
use App\RetailerProgram\Models\PointsInventoryModel;

function refundRedeemedPoints($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $invoice = $arguments[1];
    $refundProduct = $arguments[2];
    $isRefundProduct = true;
    
    try {

        if(!isset($invoice['points_by'])) { 
            $refundProduct = array($invoice['order_id']);
            $isRefundProduct = false;
        }

        $PointsInventoryModel = new PointsInventoryModel();
        $PointsInventoryModel->setCurrentUser($auth['user_id']);
        
        $PointsTransactionModel = new PointsTransactionModel();
        $PointsTransactionModel->setCurrentUser($auth['user_id']);

        $userId = $PointsTransactionModel->convertToObjectId($invoice['user_id']);
        
        $processNumber = $auth['app_prefix']."-".hexdec(uniqid().rand(0,99).chr(rand(65,70)));

        $ptsTxData = [];
        $updatePtsTx = [];
        $updatePtxInv = [];

        foreach ($refundProduct as $product) {

            $refNo = $isRefundProduct ? $invoice['order_id'].'-'.$product : $invoice['order_id'];

            $filter = [
                'user_id'=>$userId,
                'reference_no'=>$refNo,
                'status'=>'PROCESSED',
                'process_type'=>'REFUND',
            ];
            $ptsTxData = $PointsTransactionModel->DBfind($filter);
            if (!empty($ptsTxData['result'])) return;

            $filter = [
                'user_id'=>$userId,
                'reference_no'=>$refNo,
                'status'=>'PROCESSED',
                'process_type'=>'SUB',
            ];
            
            $ptsTxRedeem = $PointsTransactionModel->DBfind($filter);
            $ptsTxRedeem = $thisViewModel->objectToArray($ptsTxRedeem['result']);

            foreach ($ptsTxRedeem as $key => $value) {
                $ptsTxData[] = [
                    'points'=>$value['points'],
                    'user_id'=>$userId,
                    'reference_no'=>$refNo,
                    'description'=>'refund redeemed points',
                    'remarks'=>'refund points',
                    'status'=>'PROCESSED',
                    'transaction_month'=>$value['transaction_month'],
                    'item_code'=>$value['item_code'],
                    'process_type'=>'REFUND',
                    'process_number'=>$processNumber,
                    'process_id'=>$processNumber.'-001',
                ];

                $updatePtsTx[] = [
                    'filter' => ['_id'=>$PointsTransactionModel->convertToObjectId($value['_id'])],
                    'new_value'=>[
                        'status'=>'CANCELED'
                    ]
                ];

                $updatePtxInv[] = [
                    'custom'=>true,
                    'filter' => [
                        'user_id'=>$userId,
                        'item_code'=>$value['item_code']
                    ],
                    'new_value'=>[
                        '$inc'=>[
                            'points'=>$value['points']
                        ]
                    ]
                ];
            }
        }

        $PointsTransactionModel->insertBatch($ptsTxData);
        $PointsTransactionModel->updateBatch($updatePtsTx);
        $PointsInventoryModel->updateBatch($updatePtxInv);
        
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}