<?php
namespace App\RetailerProgram\ViewModels\CategoryViewModel;

use App\RetailerProgram\Models\CategoryModel;

function getCategoryList($arguments, $thisViewModel){

    $search = $arguments[0];

    try{
        $CategoryModel = new CategoryModel();

        $categoryList = $CategoryModel->getcategoryList($search);

        return $categoryList;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}