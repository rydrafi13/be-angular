<?php
namespace App\RetailerProgram\ViewModels\CategoryViewModel;

use App\RetailerProgram\Models\CategoryModel;

function deleteCategory($arguments, $thisViewModel)
{
    $auth = $arguments[0];
    $keyOrCode = $arguments[1];
    try {

        $CategoryModel = new CategoryModel();
        $CategoryModel->setCurrentUser($auth['user_id']);

        $findCat = $CategoryModel->findByCategoryCodeOrName($keyOrCode);
        empty($findCat['result']) ?
            $thisViewModel->sendError('Category {$keyOrCode} does not exists', 404): null;

        $ids = [];
        foreach ($findCat['result'] as $key => $value) {
            $ids[] = $CategoryModel->convertToObjectId($value->_id);
        }

        $result = $CategoryModel->removeCategory(['_id'=>['$in'=>$ids]]);
        
        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
