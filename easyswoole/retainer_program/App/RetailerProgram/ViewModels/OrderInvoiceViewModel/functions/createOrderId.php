<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

function createOrderId($arguments, $thisViewModel) {
    $prefix = $arguments[0]?($arguments[0].'-'):'';

    try {
        $append  = '';
        if (ENVIRONTMENT == 'DEVELOPMENT') {
            $append = '-DEV';
        }

        $random = chr(rand(65,70)).rand(0,99);
        $crc = strval( hexdec( uniqid().$random));

        return $prefix.$crc.$append;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}