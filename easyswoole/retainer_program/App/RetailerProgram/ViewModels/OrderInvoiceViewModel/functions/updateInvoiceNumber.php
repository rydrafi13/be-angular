<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function updateInvoiceNumber($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $option = $arguments[1];
    $bodyData = $arguments[2];

    try {
        
        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($auth['user_id']);

        $MediaViewModel = new MediaViewModel();
        $document = $MediaViewModel->uploadDocument($auth, $bodyData, "UPDATE_INVOICE");

        $SpreadsheetService = new SpreadsheetService();
        $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

        $data = [
            'user_id' => $auth['user_id'],
            'files'=>array_values($files)
        ];
        if (in_array($document['ext'], ['xls','xlsx'])) {
            switch ($option) {
                case 'invoice_no':
                    $result = updateInvoiceNumberXLS($data, $thisViewModel);
                    break;
                
                case 'po_no':
                    $result = updatePONumberXLS($data, $thisViewModel);
                    break;
            }
        } elseif (in_array($document['ext'], ['csv'])) {
            // process csv here
        }

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function updateInvoiceNumberXLS($data, &$thisViewModel) {

    try {

        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($data['user_id']);

        $requiredField = explode(",", "order_id,invoice_no");
        $currDate = $OrderInvoiceModel->convertToMongoDateTime(date('Y-m-d H:i:s'));
        $updateData = [];
        foreach ($data['files'] as $key => $value) {
            $row = $key + 1;
            foreach ($requiredField as $k => $val) {
                empty($value[$val])? $thisViewModel->sendError("field {$val} on row {$row} is required", 400): null;   
            }

            $invoice = $OrderInvoiceModel->findByOrderId($value['order_id'], ['status'=>'COMPLETED']);
            empty($invoice['result']) ?
                $thisViewModel->sendError("order data for order_id {$value['order_id']} not found", 404) :
                $invoice = $thisViewModel->objectToArray($invoice['result'][0]);

                $updateData[] = [
                    'filter'=>['_id'=>$OrderInvoiceModel->convertToObjectId($invoice['_id'])],
                    'new_value'=>[
                        'invoice_no'=>[
                            'value'=>$value['invoice_no'],
                            'created_date'=>$currDate,
                            'created_by'=>$data['user_id']
                        ]
                    ]
                ];
        }

        if (!empty($updateData)) {
            $result = $OrderInvoiceModel->updateBatch($updateData);
        } else {
            $result = ['result'=>['status'=>'no data updated']];
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function updatePONumberXLS($data, &$thisViewModel) {

    try {

        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($data['user_id']);

        $requiredField = explode(",", "order_id,po_no");
        $currDate = $OrderInvoiceModel->convertToMongoDateTime(date('Y-m-d H:i:s'));
        $updateData = [];
        foreach ($data['files'] as $key => $value) {
            $row = $key + 1;
            foreach ($requiredField as $k => $val) {
                empty($value[$val])? $thisViewModel->sendError("field {$val} on row {$row} is required", 400): null;   
            }

            $invoice = $OrderInvoiceModel->findByOrderId($value['order_id'], ['status'=>['COMPLETED','PROCESSED']]);
            empty($invoice['result']) ?
                $thisViewModel->sendError("order data for order_id {$value['order_id']} not found", 404) :
                $invoice = $thisViewModel->objectToArray($invoice['result'][0]);

                $updateData[] = [
                    'filter'=>['_id'=>$OrderInvoiceModel->convertToObjectId($invoice['_id'])],
                    'new_value'=>[
                        'po_no'=>[
                            'value'=>$value['po_no'],
                            'created_date'=>$currDate,
                            'created_by'=>$data['user_id']
                        ]
                    ]
                ];
        }

        if (!empty($updateData)) {
            $result = $OrderInvoiceModel->updateBatch($updateData);
        } else {
            $result = ['result'=>['status'=>'no data updated']];
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}