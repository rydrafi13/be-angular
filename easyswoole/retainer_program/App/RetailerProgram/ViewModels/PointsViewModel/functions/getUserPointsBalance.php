<?php
namespace App\RetailerProgram\ViewModels\PointsViewModel;

use App\RetailerProgram\Models\PointsInventoryModel;

function getUserPointsBalance($arguments, $thisViewModel) {
    $auth = $arguments[0];

    try {
        $PointsInventoryModel = new PointsInventoryModel();

        $search = [
            'get_total_points'=>true,
            'points_exists'=>true,
            'expiry_date'=>date("Y-m"),
            'status'=>'ACTIVE'
        ];
        $ptsItem = $PointsInventoryModel->getPointByUserId($auth['subject_id'], $search);

        !empty($ptsItem['result']) ?
            $points = $ptsItem['result'][0]->points:
            $points = 0;

        $result = [
            'result' => [
                'points_balance' => $points
            ]
        ];

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}