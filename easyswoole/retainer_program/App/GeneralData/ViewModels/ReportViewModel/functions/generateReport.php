<?php
namespace App\GeneralData\ViewModels\ReportViewModel;

use App\GeneralData\ViewModels\MediaViewModel;
use App\GeneralData\Models\ConfigurationModel;

function generateReport($arguments, $thisViewModel) {
    $dataReport = $arguments[0];
    $remarks = $arguments[1];
    $reportType = $arguments[2];

    empty($remarks) ? $remarks = 'GENERAL' : null;
    empty($reportType)? $reportType = 'csv' : null;
    try {

        if ($reportType == 'csv') {
            $result = $thisViewModel->convertResultToCsv($dataReport);
        } elseif ($reportType == 'xlsx') {
            $result = $thisViewModel->convertResultToXlsx($dataReport);
        } else {
            $thisViewModel->sendError("report type not supported", 400);
        }

        $fileInfo = [
            'filename'=>$result['filename'],
            'file_path'=>$result['output_dir'],
            'folder'=> 'system'.DIRECTORY_SEPARATOR.'reports'.DIRECTORY_SEPARATOR.$reportType.DIRECTORY_SEPARATOR.strtolower($remarks).DIRECTORY_SEPARATOR.date('Y-m-d').DIRECTORY_SEPARATOR
        ];

        $MediaViewModel = new MediaViewModel();
        $ConfigurationModel = new ConfigurationModel();

        $cloudinaryConfig = $ConfigurationModel->findByKeyName("cloudinary-configuration");
        $cloudinaryConfig = $cloudinaryConfig['result'][0]->values;
        $result = $MediaViewModel->uploadToCloudinary('document', $fileInfo, $cloudinaryConfig, null, null);

        $result = [
            'result'=>[
                'success'=>true,
                'url'=>$result['secure_url'],
            ]
        ];

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}