<?php 
namespace App\GeneralData\ViewModels\CourierViewModel;

use Services\Curl;
function getSAPRegion($arguments, $thisViewModel)
{
    $getBody = $arguments[0];
    try {
        $Curl = new Curl();

        $param = $thisViewModel->getCourier("REALS-SAP");

        $setParam['url'] = $param['api_url']['origin_destination'];
        $setParam['method'] = 'GET';
        $setParam['headers'] = [
                                "api-key: ".$param['api_key']['tracking'],
                                'Content-Type: application/json'
                               ];

        $result = $Curl->sendRequest($setParam);

        $dataLog = ['request_type'=>'SAP_GET_REGION_LIST',
                    'request_header'=>$setParam['headers'],
                    'request_body'=>[],
                    'request_url'=>$setParam['url'],
                    'response'=>$result,
                    'response_code'=>$result['response_code'],
                    'request_date'=>date('y-m-d h:i:s')
                    ];

        write_log($dataLog, 'COURIER_LOG/SAP');

        return $result['result'];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}