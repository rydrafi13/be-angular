<?php 
namespace App\GeneralData\ViewModels\CourierViewModel;

use App\GeneralData\Models\ShippingRegionModel;

function requestPickup($arguments, $thisViewModel) 
{
    $deliveryData = $arguments[0];
    $courier = $arguments[1];
    $deliveryOptions = $arguments[2];

    try {
        $orgAddress = $deliveryData['origin']['address'];
        $destAddress = $deliveryData['destination']['address'];
        $dimensions = $deliveryData['dimensions'];

        ( $deliveryData['weight'] >= $deliveryData['volumetric_weight'] ) ?
            $packageWeight = $deliveryData['weight'] :
            $packageWeight = $deliveryData['volumetric_weight'];
        $packageValue = $deliveryData['total_item_value'];
        $packageQty = $deliveryData['total_item_qty'];
        $packageDimension = $dimensions['length']."x".$dimensions['width']."x".$dimensions['height'];

        /* $packageWeight = 0;
        $packageValue = 0;
        $packageQty = 0;
        foreach ($deliveryData['products'] as $key => $value) {
            if ($value['type'] == 'product' || $value['type'] == 'voucher') {
                $packageWeight += ($value['volume_weight'] * $value['quantity']);
                $packageQty += $value['quantity'];
                $packageValue += $value['sku_value'];
            }
        } */

        /* $ShippingRegionModel = new ShippingRegionModel();

        $filter = [
            'usage'=>'origin',
            'courier_code'=>$deliveryData['courier'],
            'region_code'=>$deliveryData['origin']['code']
        ];
        $getOrigin = $ShippingRegionModel->findByRegionCode($filter);
        if (empty($getOrigin['result'])) {
            $thisViewModel->sendError("shipping origin not found", 400);
        }

        $filter = [
            'usage'=>'destination',
            'courier_code'=>$deliveryData['courier'],
            'region_code'=>$deliveryData['destination']['code']
        ];
        $getDest = $ShippingRegionModel->findByRegionCode($filter);
        if (empty($getDest['result'])) {
            $thisViewModel->sendError("shipping destination not found", 400);
        } */

        $trackingHistory = [];
        $lastStatus = [];
        switch ($deliveryData['courier']) {
            case 'REALS-SAP':
                $deliveryService = $deliveryData['delivery_service'];

                $insurance = false; // no insurance
                $packingType = false;
                $specialInstruction = false;
                if ($deliveryOptions['insurance'] == true) {
                    $insurance = true;
                    $insuranceType = 'INS01';
                }
                switch ($deliveryOptions['packing_type']) {
                    case '01':
                        $packingType = 'ACH02'; // packing kayu
                        break;
                    
                    case '02':
                        $packingType = 'ACH03'; // packing kardus / busa
                        break;

                    case '03':
                        $packingType = 'ACH04'; // packing full
                        break;
                }
                if (!empty($deliveryOptions['instruction'])) {
                    $specialInstruction = $deliveryOptions['instruction'];
                }

                if ($deliveryData['delivery_type'] == 'gold') {
                    $insurance = true;
                    $insuranceType = 'INS01';

                    $customerCode = $courier['customer_code']['special'];
                    $typeSpecial = true;
                    $packingType = false;
                } else {
                    $customerCode = $courier['customer_code']['regular'];
                    $typeSpecial = false;
                }

                // (ENVIRONTMENT == "DEVELOPMENT") ? $customerCode = $courier['customer_code']['regular'] : null;

                $data = [
                    'customer_code'=>$customerCode,
                    'reference_no'=>$deliveryData['reference_no'],
                    'pickup_name'=>$orgAddress['name'],
                    'pickup_address'=>$orgAddress['address'],
                    'pickup_phone'=>$orgAddress['cell_phone'],
                    'pickup_district_code'=>$courier['origin']['region_code'],
                    'service_type_code'=>$courier['delivery_service'][$deliveryService],
                    'quantity'=>1,  // one koli in one shipment
                    'total_item'=>1,  // total item in one koli
                    'weight'=>$packageWeight,
                    'volumetric'=>$packageDimension,
                    'shipment_type_code'=>'SHTPC',
                    'item_value'=>$packageValue,
                    'insurance_flag'=>1,        // non insurance
                    'insurance_value'=>$packageValue,
                    'cod_flag'=>1,              // non COD
                    'shipper_name'=>$orgAddress['name'],
                    'shipper_address'=>$orgAddress['address'],
                    'shipper_phone'=>$orgAddress['cell_phone'],
                    'destination_district_code'=>$courier['destination']['region_code'],
                    'receiver_name'=>$destAddress['name'],
                    'receiver_address'=>$destAddress['address'],
                    'receiver_phone'=>$destAddress['cell_phone'],
                    'receiver_postal_code'=>$destAddress['postal_code'],
                ];
                if ($insurance) {
                    $data['insurance_flag'] = 2;
                    $data['insurance_type_code'] = $insuranceType;
                }
                if ($packingType) {
                    $data['packing_type_code']  = $packingType;
                }
                if ($specialInstruction) {
                    $data['special_instruction']  = $specialInstruction;
                }
    
                $setParam = ['file' => $data];
                
                $requestPickup = $thisViewModel->courierPickUpSAP($setParam, $typeSpecial);

                if($requestPickup['status'] == '200' || $requestPickup['status'] == 'success'){
                    $awbNumber = $requestPickup['data']['awb_no'];
    
                    $setParam = [
                        'awb_number' => $awbNumber,
                        'tracking' => 'awb_no'
                    ];
                    $trackAWB = $thisViewModel->courierTrackingSAP($setParam);
    
                    if(!empty($trackAWB)){
                        $trackingHistory = $trackAWB;
                        $lastStatus = end($trackAWB);
    
                        if(!empty($lastStatus) && strpos($lastStatus['rowstate_name'], "ENTRI VERIFIED") !== false){
                            $setOnDelivery = true;
                        }
                    }
                }

                $result = [
                    'tracking_history'=>$trackingHistory,
                    'last_status' => $lastStatus,
                    'awb_number'=>$awbNumber,
                    // 'origin_branch'=>$courier['origin']['origin_branch'],
                    // 'dest_branch'=>$courier['destination']['dest_branch']
                ];
                break;
            
            default:
                # code...
                break;
        }

        empty($awbNumber) ?
            $thisViewModel->sendError("courier airway bill number not available", 400) : null;
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}