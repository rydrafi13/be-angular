<?php
namespace App\GeneralData\ViewModels\CourierViewModel;

use App\GeneralData\Models\CourierListModel;

function getCourierList($arguments, $thisViewModel)
{
    $courierCode = $arguments[0];
    try {
        $CourierListModel = new CourierListModel();

        $courier = $CourierListModel->getAvailableCourierList($courierCode);

        if (empty($courier['result'])) {
            $thisViewModel->sendError("No Courier Found", 404);
        }
        $courier = $thisViewModel->objectToArray($courier['result']);

        return $courier;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
