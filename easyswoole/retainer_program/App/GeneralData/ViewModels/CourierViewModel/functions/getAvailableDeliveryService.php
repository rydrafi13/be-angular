<?php
namespace App\GeneralData\ViewModels\CourierViewModel;

function getAvailableDeliveryService($arguments, $thisViewModel)
{
    $courier = $arguments[0];
    $origin = $arguments[1];
    $destination = $arguments[2];
    $weight = $arguments[3] ?: 1;
    $typeSpecial = $arguments[4] ?: false;
    $packageValue = $arguments[5];

    try {

        switch ($courier) {
            case 'REALS-SAP':
                $request = [
                    'origin'=>$origin,
                    'destination'=>$destination,
                    'weight'=>$weight,
                ];
                if ($typeSpecial) {
                    empty($packageValue) ?
                        $thisViewModel->sendError("package value must be provided", 400) : null;
                }
                !empty($packageValue) ? $request['item_value'] = $packageValue : null;

                $shipment = $thisViewModel->getSAPShipmentCost($request, $typeSpecial);

                $deliveryService = $shipment['price'];
                break;
            
            default:
                # code...
                break;
        }

        

        return $deliveryService;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
