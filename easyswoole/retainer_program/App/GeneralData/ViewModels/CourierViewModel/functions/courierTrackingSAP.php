<?php 
namespace App\GeneralData\ViewModels\CourierViewModel;

use Services\Curl;
function courierTrackingSAP($arguments, $thisViewModel)
{   
    $getBody = $arguments[0];
    try{
        $Curl = new Curl();

        $param = $thisViewModel->getCourier("REALS-SAP");

        if ($getBody['tracking']=='awb_no') {
            $body=['awb_no'=>$getBody['awb_number']];
            $data = http_build_query($body);
            $param['url'] = $param['api_url']['tracking_by_awb'].'?'.$data;
        } elseif ($getBody['tracking']=='ref_no'){
            $body=['reference_no'=>$getBody['reference_no']];
            $data = http_build_query($body);
            $param['url'] = $param['api_url']['tracking_by_ref_no'].'?'.$data;
        }
        $param['method'] = 'GET';
        $param['headers'] = [
                            "api-key: ".$param['api_key']['tracking'],
                            'Content-Type: application/json'
                            ];

        $result = $Curl->sendRequest($param);

        $dataLog = ['request_type'=>'SAP_TRACKING',
                    'request_header'=>$param['headers'],
                    'request_body'=>[],
                    'request_url'=>$param['url'],
                    'response'=>$result,
                    'response_code'=>$result['response_code'],
                    'request_date'=>date('y-m-d h:i:s')
                    ];

        write_log($dataLog, 'COURIER_LOG/SAP');

        return $result['result'];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}