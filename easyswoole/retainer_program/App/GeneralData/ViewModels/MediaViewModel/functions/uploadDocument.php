<?php
namespace App\GeneralData\ViewModels\MediaViewModel;

use App\GeneralData\Models\MediaUploadModel;
use App\GeneralData\Models\ConfigurationModel;

function uploadDocument($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $document = $arguments[1];
    $remarks = $arguments[2];

    try {
        empty($remarks) ? $remarks = 'GENERAL' : null;

        if ($document['size'] > 3*1024 *1024) {
            $thisViewModel->sendError("Maximum image size is 3 MB", 400);
        }

        $whitelistType = [
            'xlsx'=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xls'=>'application/vnd.ms-excel', 
            'csv'=>'text/csv',
            'pdf'=>'application/pdf'
        ];

        foreach ($whitelistType as $key => $value) {
            $pos = strpos($document['name'], '.'.$key, -5);
            if ($pos !== false) {
                $len = strlen($key)+1;
                $document['name'] = substr($document['name'], 0, strlen($document['name']) - $len);
                break;
            }
        }

        $fileType = implode(", ", array_flip($whitelistType));

        $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
        $fileMimeType = finfo_file($fileinfo, $document['tmp_name']);
        if (!in_array($fileMimeType, $whitelistType)) {
            $error = true;
        }
        if($error){
            $fileMimeType = mime_content_type($document['tmp_name']);
            if ($fileMimeType == 'text/plain' && $document['type'] == 'text/csv') {
                $fileMimeType = 'text/csv';
            }

            if (!in_array($fileMimeType, $whitelistType)) {
                if (in_array($document['type'], $whitelistType)) {
                    $fileMimeType = $document['type'];
                } elseif(isset($whitelistType[$document['ext']])){
                    $fileMimeType = $whitelistType[$document['ext']];
                } else {
                    $thisViewModel->sendError("Only file with {$fileType} type are allowed", 400);
                }
            }
        }
        $document['type'] = $fileMimeType;
        $document['ext'] = array_keys($whitelistType, $fileMimeType, true)[0];

        $document['file_path'] = $document['tmp_name'];
        $document['filename'] = $document['name'].uniqid();

        $document['folder'] = $auth['type'].DIRECTORY_SEPARATOR."document".DIRECTORY_SEPARATOR.strtolower($remarks).DIRECTORY_SEPARATOR.date('Y-m-d').DIRECTORY_SEPARATOR;

        $ConfigurationModel = new ConfigurationModel();
        $cloudinaryConfig = $ConfigurationModel->findByKeyName("cloudinary-configuration");
        $cloudinaryConfig = $cloudinaryConfig['result'][0]->values;
        $result = $thisViewModel->uploadToCloudinary('document', $document, $cloudinaryConfig, null, null);

        $document['result'] = $result['secure_url'];

        $MediaUploadModel = new MediaUploadModel();
        $MediaUploadModel->setCurrentUser($auth['user_id']);

        $document['process_no'] = $auth['app_prefix']."-".strval(hexdec(uniqid().rand(0,99).chr(rand(65,70))));
        $mediaInfo = [
            'file_name'=>$document['name'],
            'file_size'=>$document['size'],
            'file_type'=>$document['type'],
            'file_format'=>$document['ext'],
            'upload_type'=>'document',
            'process_no'=>$document['process_no'],
            'status'=>'UPLOADED',
            'remarks'=>$remarks,
        ];

        $MediaUploadModel->insert($mediaInfo);
        
        return $document;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}