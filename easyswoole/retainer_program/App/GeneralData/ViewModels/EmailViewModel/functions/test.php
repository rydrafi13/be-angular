<?php
namespace App\GeneralData\ViewModels\EmailViewModel;

use App\GeneralData\Models\EmailModel;
function test($arguments, $thisViewModel) {
    
    try {
        
        $mailData = [
            'test_data'=>'Hello World!'
        ];
        $emailModel = new EmailModel();
        $insertMail = $emailModel->insert($mailData);
        
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}