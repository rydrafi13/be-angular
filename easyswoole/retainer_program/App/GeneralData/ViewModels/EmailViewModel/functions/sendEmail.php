<?php
namespace App\GeneralData\ViewModels\EmailViewModel;

use App\GeneralData\Models\EmailModel;
use Services\Email;
function sendEmail($arguments, $thisViewModel) {
    
    $mailData = $arguments[0];
    try {

        $from = $mailData['sender_email']?:'REALS Support<admin.reals@cls-indo.co.id>';
        $cc = $mailData['cc'];
        $to = $mailData['recipient_email'];
        $subject = $mailData['subject'];
        $body = $mailData['body'];
        $tag = isset($mailData['tag']) ? $mailData['tag'] : 'Transaction';
        $attachment = $mailData['attachment']?:[];

        $Email = new Email();
        $returnResult = $Email->sendMailBy($to, $body, $from, $subject, $cc, $tag, $attachment);
        
        $mailData['result'] = $returnResult;
        $emailModel = new EmailModel();
        $sendMail = $emailModel->insert($mailData);

        return $returnResult;
        
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}