<?php
namespace App\GeneralData\ViewModels\AdministrativeRegionViewModel;

use App\GeneralData\Models\AdministrativeRegionModel;

function searchRegion($arguments, $thisViewModel) {
    $bodyData = $arguments[0];

    try { 

        $AdministrativeRegionModel = new AdministrativeRegionModel();

        $bodyData['search'] = trim($bodyData['search']);
        $bodyData['region'] = trim($bodyData['region']);

        switch ($bodyData['region']) {
            case 'province':
                $regionList = $AdministrativeRegionModel->groupByProvince($bodyData['search']);
                break;
            
            case 'city':
                $bodyData['province_code'] = trim($bodyData['province_code']);
                empty($bodyData['province_code']) ? 
                    $thisViewModel->sendError("parameter province_code is required", 400) : null;

                $regionList = $AdministrativeRegionModel->groupByCity($bodyData['province_code'], $bodyData['search']);
                break;

            case 'subdistrict':
                $bodyData['city_code'] = trim($bodyData['city_code']);
                empty($bodyData['city_code']) ? 
                    $thisViewModel->sendError("parameter city_code is required", 400) : null;

                $regionList = $AdministrativeRegionModel->groupBySubdistrict($bodyData['city_code'], $bodyData['search']);
                break;

            case 'village':
                $bodyData['subdistrict_code'] = trim($bodyData['subdistrict_code']);
                empty($bodyData['subdistrict_code']) ? 
                    $thisViewModel->sendError("parameter subdistrict_code is required", 400) : null;

                $regionList = $AdministrativeRegionModel->groupByVillage($bodyData['subdistrict_code'], $bodyData['search']);
                break;
        }

        $result = ['result'=>$thisViewModel->objectToArray($regionList['result'])];

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}