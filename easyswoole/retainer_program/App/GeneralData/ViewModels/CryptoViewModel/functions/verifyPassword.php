<?php
namespace App\GeneralData\ViewModels\CryptoViewModel;

function verifyPassword($arguments, $thisViewModel) {
    $password = $arguments[0];
    $hash = $arguments[1];

    try {

        if( empty($password) || empty($hash) ) {
            $thisViewModel->sendError("Invalid Password", 400);
        }
        
        return password_verify($password, $hash);
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}