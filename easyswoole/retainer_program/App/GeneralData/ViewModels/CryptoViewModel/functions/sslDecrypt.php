<?php
namespace App\GeneralData\ViewModels\CryptoViewModel;

function sslDecrypt($arguments, $thisViewModel) {
    $data = $arguments[0];
    $cipher = $arguments[1]?:$thisViewModel->openSslAlgo;
    $key = $arguments[2]?:$thisViewModel->openSslKey;
    $iv = $arguments[3]?:$thisViewModel->openSslIv;
    // $tag = $arguments[4]?:$thisViewModel->tag;

    try{
        empty($data) ? $thisViewModel->sendError("secret must not be empty", 400) : null;

        if (in_array($cipher, openssl_get_cipher_methods())) {
            $data = base64_decode($data);
            $originalMsg = openssl_decrypt($data, $cipher, $key, $options=0, $iv);
        }
        return $originalMsg;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}