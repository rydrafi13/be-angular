<?php
namespace App\GeneralData\ViewModels;

class CryptoViewModel extends ViewModelMaster{
    public $jwtKey = JWT_KEY;

    public $aesKey = ENCRYPTION_KEY;

    public $openSslAlgo = OPEN_SSL_ALGO;
    public $openSslKey = OPEN_SSL_KEY;
    public $openSslIv = OPEN_SSL_IV;
}