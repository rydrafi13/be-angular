<?php
namespace App\UserManagement\ViewModels\AdminGroupViewModel;

use App\UserManagement\Models\AdminGroupModel;
use App\UserManagement\Models\AccessListModel;
use App\UserManagement\Models\AppListModel;

function addNewGroup($arguments, $thisViewModel) {
    $bodyData = $arguments[0];
    try {
        $AdminGroupModel = new AdminGroupModel();

        $bodyData['group_name'] = trim($bodyData['group_name']);
        $find = $AdminGroupModel->findByGroupName($bodyData['group_name']);
        if (!empty($find['result'])) {
            $thisViewModel->sendError("admin group {$bodyData['group_name']} already exists", 409);
        }

        if (!empty($bodyData['access_list'])) {
            $AccessListModel = new AccessListModel();
            $AppListModel = new AppListModel();

            foreach ($bodyData['access_list'] as $key => $value) {
                $findApp = $AppListModel->findByAppLabel($key, 'ACTIVE');
                if (empty($findApp['result'])){
                    $thisViewModel->sendError("App label is invalid", 400);
                }

                if (empty($value) || !is_array($value)) {
                    $thisViewModel->sendError("access menu list be an array", 400);
                }

                $filter = [
                    'name'=>$value,
                    'app_label'=>$key,
                    'status'=>'ACTIVE'
                ];
                $findAccess = $AccessListModel->getAccessNameList($filter);
                if (empty($findAccess['result'])) {
                    $thisViewModel->sendError("Access menu does not exists", 404);
                }
                foreach ($value as $sk => $sval) {
                    if (!in_array($sval, $findAccess['result'][0]->name)) {
                        $thisViewModel->sendError("access {$sval} is invalid", 400);
                    }
                }

                $bodyData['access_list'][$key] = implode("|", $value);
            }
        }

        $bodyData['status'] = 'ACTIVE';

        $result = $AdminGroupModel->insert($bodyData);

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}