<?php
namespace App\UserManagement\ViewModels\AdminViewModel;

use App\UserManagement\Models\AdminModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function changeAdminPassword($arguments, $thisViewModel) {
    $bodyData = $arguments[0];
    try {

        $AdminModel = new AdminModel();

        $result = $AdminModel->findByUsernameOrEmail(['username'=>$bodyData['username'], 'status'=>'ACTIVE']);
        if (empty($result['result'])) {
            $thisViewModel->sendError("admin account does not exists", 404);
        }

        $CryptoViewModel = new CryptoViewModel();

        $result = $AdminModel->update(
            ['_id'=>$result['result'][0]->_id],
            ['password'=>$CryptoViewModel->generatePassword($bodyData['password'], 16)]
        );

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}