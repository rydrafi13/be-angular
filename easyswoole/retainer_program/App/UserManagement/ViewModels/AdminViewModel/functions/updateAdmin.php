<?php
namespace App\UserManagement\ViewModels\AdminViewModel;

use App\UserManagement\Models\AdminModel;
use App\UserManagement\Models\AdminGroupModel;

function updateAdmin($arguments, $thisViewModel) {
    $username = $arguments[0];
    $bodyData = $arguments[1];
    try {
        $acceptedBodyData = 'email,full_name,group_name,status';

        $AdminModel = new AdminModel();
        $find = $AdminModel->findByUsernameOrEmail(['username'=>$username]);

        if (empty($find['result'])) {
            $thisViewModel->sendError("admin account does not exists", 404);
        }

        $acceptedField = explode(",", $acceptedBodyData);
        foreach ($bodyData as $key => $value) {
            if(!in_array($key, $acceptedField)) {
                unset($bodyData[$key]);
            }
        }

        if (!empty($bodyData['group_name'])) {
            $AdminGroupModel = new AdminGroupModel();

            $group = $AdminGroupModel->findByGroupName($bodyData['group_name'], 'ACTIVE');

            if (empty($group['result'])){
                $thisViewModel->sendError("admin group {$bodyData['group_name']} does not exists", 404);
            }

            $bodyData['group_id'] = $group['result'][0]->_id;
        }

        $result = $AdminModel->update(
            ['_id'=>$find['result'][0]->_id],
            $bodyData
        );

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}