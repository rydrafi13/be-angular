<?php
namespace App\UserManagement\ViewModels\AppListViewModel;

use App\UserManagement\Models\AppListModel;

function updateApp($arguments, $thisViewModel) {
    $getParams = $arguments[0];
    $bodyData = $arguments[1];
    try {
        $acceptedBodyData = 'app_name,app_description,status';

        $AppListModel = new AppListModel();
        $find = $AppListModel->findByAppLabel($getParams);

        if (empty($find['result'])) {
            $thisViewModel->sendError("app label does not exists", 404);
        }

        $acceptedField = explode(",", $acceptedBodyData);
        foreach ($bodyData as $key => $value) {
            if(!in_array($key, $acceptedField)) {
                unset($bodyData[$key]);
            }
        }

        $result = $AppListModel->update(
            ['_id'=>$find['result'][0]->_id],
            $bodyData
        );

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}