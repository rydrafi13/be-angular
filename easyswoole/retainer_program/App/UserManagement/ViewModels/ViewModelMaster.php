<?php
namespace App\UserManagement\ViewModels;

class ViewModelMaster {

    public function __call($funcName, $arguments)
    {
        $currentClass = get_called_class();

        // Note: value of $name is case sensitive.
        if($this->loadFunction($funcName)){
            $function = "$currentClass\\$funcName";
            
            eval("\$function = \$function(\$arguments, \$this);");
            return $function;
        }
    }

    private function loadFunction($functionName){
        $namespace = explode("\\", get_called_class());

        $currentDir     = PROJECT_PATH;
        $vmDir          = $namespace[0].DIRECTORY_SEPARATOR.$namespace[1].DIRECTORY_SEPARATOR.$namespace[2].DIRECTORY_SEPARATOR.$namespace[3];
        $functionDir    = 'functions';
        $functionFile   = $currentDir.$vmDir.DIRECTORY_SEPARATOR.$functionDir.DIRECTORY_SEPARATOR.$functionName.'.php';

        if(!file_exists($functionFile)) {
            throw new \Exception("error ".$functionName." doesn't exists", 1);
        }
        @include_once($functionFile);

        return true;
    }

    function objectToArray($d)
    {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }
            
        if (is_array($d)) {
            /*
            * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return array_map(array($this,__FUNCTION__), $d);
        } else {
            // Return array
            return $d;
        }
    }

    function sendResult($result=''){
        global $requestSession;
        /** Setup Header */
        // header("Content-type:application/json");
        // header("Access-Control-Allow-Origin: *");
        // header("Access-Control-Allow-Headers: *");
        // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        
        // http_response_code(404);
        
        if($this->error !=false){
            $return= array(
                'error'=> $this->error,
                'error_code'=>$this->error_code
            );
            http_response_code($return['error_code']);
        }
        elseif(isset($result['error']) && $result['error'] !== false){
            $return= array(
                'error'=> $result['error'],
                'error_code'=>$result['error_code']
            );
            http_response_code($return['error_code']);
        }
        else{
            $return= array(
                'result'=>$result['result'],
                'error'=>false
            );
            http_response_code(200);
        }
        echo json_encode($return);
        write_log([
                    'msg'=>'--| END-REQUEST : '.$requestSession.' |--'.date('y-m-d h:i:s'),
                    'response'=>$return
                ],'TRACE');
        exit(1);
    }

    function sendError($errorMessage, $errorCode, \Throwable $e = null){
        // $this->error_code = $e->getMessage();
        // $this->error = $e->getCode();
        
        if ($e && get_class($e) == "Error") {
            throw new \Error($errorMessage, $errorCode, $e);
        } else {
            throw new \Exception($errorMessage, $errorCode, $e);
        }
    }
}