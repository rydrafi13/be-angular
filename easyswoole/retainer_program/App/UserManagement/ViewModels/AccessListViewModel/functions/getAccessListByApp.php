<?php
namespace App\UserManagement\ViewModels\AccessListViewModel;

use App\UserManagement\Models\AccessListModel;

function getAccessListByApp($arguments, $thisViewModel) {
    try {

        $AccessListModel = new AccessListModel();
        $result = $AccessListModel->getAccessListByApp();

        $returnResult = [];
        if (!empty($result['result'])) {
            $result = $thisViewModel->objectToArray($result['result']);
            foreach ($result as $key => $value) {
                $returnResult[$value['_id']] = $value['access_list'];
            }
        }

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}