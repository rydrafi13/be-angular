<?php
use App\UserManagement\ViewModels\AppListViewModel;

class AppListController extends MasterController{

    function firstLoad(){

    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");
            $request = json_decode($srv->get('GET.request'), 1);
            
            $search = $request['search'] ?: [];

            $AppListViewModel = new AppListViewModel();

            $result = $AppListViewModel->findApp($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function addNewApp($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $getBody = $this->getBody();

            $AppListViewModel = new AppListViewModel();

            $result = $AppListViewModel->addNewApp($getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateApp($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $appLabel = $params['app_label'];
            $getBody = $this->getBody();

            $AppListViewModel = new AppListViewModel();

            $result = $AppListViewModel->updateApp($appLabel, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function deleteApp($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $appLabel = $params['app_label'];

            $AppListViewModel = new AppListViewModel();

            $result = $AppListViewModel->deleteApp($appLabel);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

}