<?php
use App\UserManagement\ViewModels\AuthViewModel;

class MasterController {
    public $srv;
    public $userID;

    public $service;
    public $error = false;
    public $error_code = 400;
    public $log_type = 'TRACE';
    public $log_enabled = false;
    public $isRestricted = true;

    public $expectedErrorResult = [];
    public $expectedSuccessResult = [];
    public $expectedRequiredBodyParams = [];
    public $hiddenMessage = false;
    public $authentication;
    public $token;
    public $payload;
    public $getBody;

    public $loadConfig = true;

    public function __construct($swooleSrv)
    {
        global $selectedDB, $requestSession;
        date_default_timezone_set(TZ);

        $this->srv = $swooleSrv;
        $this->expectedResultArray = [
            'error' => expectedResultString(),
            'error_code' => expectedResultNumber(),
        ];

        $this->getBody = json_decode($this->srv->get('BODY')->getContents(), 1);

        if (method_exists($this, 'firstLoad')) {
            $this->firstLoad();
        }

        if (!$requestSession) {
            $requestSession = hash('sha256', uniqid().date('ymdhis').rand());
        }
        loadClass();

        $this->service = (object) [];
        if ($this->isRestricted) {
            // $this->authorizationValidation();
        }

        //write_log everytime user accessing the API
        write_log('--| START-REQUEST : '.$requestSession.' |-- '.date('y-m-d h:i:s'), 'TRACE');
    }

    public function getUserToken()
    {
        return $this->token;
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function getBody()
    {
        return $this->getBody;
    }

    public function getHeaders($headerName = '')
    {
        $headers = $this->srv->get('HEADERS');
        if (!empty($headerName)) {
            if (isset($headers[$headerName])) {
                return $headers[$headerName];
            } else {
                return false;
            }
        }

        return $headers;
    }

    public function authorize(...$permissions) // call to this function must be inside try catch throwable block
    {
        empty($permissions) ? $permissions[] = "member": null;

        $token = $this->getHeaders('Authorization');
        if (empty($token))
            $token = $this->getHeaders('authorization');
        $requestMethod = $this->srv->get('request_method');

        $uriParams = $this->srv->get('query_params');
        
        $pos = strpos($this->srv->get('request_uri'), '/v1');
        $requestPath = substr($this->srv->get('request_uri'), $pos);

        if (!empty($uriParams)) {
            $find = [];
            $replace = [];
            foreach ($uriParams as $key => $value) {
                $find[] = $value;
                $replace[] = '@'.$key;
            }
            $requestPath = str_replace($find, $replace, $requestPath);
        }

        $endpoint = $requestMethod." ".$requestPath;

        $appLabel = $this->getHeaders('app_label')?:$this->getHeaders('app-label');

        $access = [
            'endpoint'=>$endpoint,
            'app_label'=>$appLabel
        ];
        if ($token === false) {
            $token = $this->srv->get('GET.authorization');
            if (empty($token)) {
                write_log('--| Authorization-Request : '.json_encode($this->getHeaders()).' |-- '.date('y-m-d h:i:s'), 'ERROR');
                throw new Exception('Restricted area no authorization code', 400);
            }
        }

        $AuthViewModel = new AuthViewModel();

        $this->payload = $AuthViewModel->authorize($token, $permissions, $access);
    }   

    public function sendError(Throwable $e) // call to this function must be inside catch throwable block
    {
        $this->errorCode = $e->getCode();
        $this->error = $e->getMessage();

        $this->log_type = 'ERROR';

        if ($this->errorCode == 500) {
            // $this->error = "Something is wrong with our server, please try again later";

            $this->log_type = "DBERR";
        } elseif (get_class($e) == "Error") {

            // $this->error = "Something is wrong with our server, please try again later";
            $this->error_code = 400;

            $this->log_type = 'FATAL';
        }

        return $this->sendResult();
    }

    public function sendResult($result = '') // call to this function must be inside try catch throwable block
    {
        global $requestSession;

        if ($this->error != false) {
            $return = [
                'error' => $this->error,
                'error_code' => $this->error_code,
            ];
            $log = ['error' => $return];
        } elseif (isset($result['error']) && $result['error'] !== false) {
            $return = [
                'error' => $result['error'],
                'error_code' => $result['error_code'],
            ];
            $log = ['error' => $return];
        } else {
            $return = [
                'result' => $result['result'],
                'error' => false,
            ];
            $log = ['response' => $return];
        }

        if (isset($return['result'])) {
            $this->checkAcceptedResponse($return);
        }

        write_log(
            ['msg' => '--| END-REQUEST : '.$requestSession.' |--'.date('y-m-d h:i:s'), $log],
            $this->log_type
        );
    
        return $return;
    }

    public function checkAcceptedRequiredBodyParams($response) // call to this function must be inside try catch throwable block
    {
        $_getBody = $this->getBody();

        $this->expectedRequiredBodyParams = $response;

        $successResponse = $_getBody;

        if (gettype($successResponse) == 'object') {
            $successResponse = json_decode(json_encode($successResponse), 1);
        }

        if (count($this->expectedRequiredBodyParams) == 0) {
            return true;
        }

        foreach ($this->expectedRequiredBodyParams as $accKey => $accValue) {
            if (isset($accValue->required)) {
                if (!isset($successResponse[$accKey])) {
                    throw new Exception('expected '.$accKey.' Not Found '.json_encode($response), 409);
                }

                $currentDataType = gettype($successResponse[$accKey]);
                if ($currentDataType != $accValue->expectedDataType) {
                    throw new Exception('unexpected Data type '.($currentDataType)." for {$accKey} expect {$accValue->expectedDataType} ".json_encode($response), 409);
                }
            }
        }
    }

    private function checkAcceptedResponse($response) // call to this function must be inside try catch throwable block
    {
        $successResponse = $response['result'];
        if (gettype($successResponse) == 'object') {
            $successResponse = json_decode(json_encode($successResponse), 1);
        }

        if (count($this->expectedSuccessResult) == 0) {
            return true;
        }

        foreach ($this->expectedSuccessResult as $accKey => $accValue) {
            if (isset($accValue->required)) {
                if (!isset($successResponse[$accKey])) {
                    throw new Exception('expected '.$accKey.' Not Found '.json_encode($response), 409);
                }

                $currentDataType = gettype($successResponse[$accKey]);
                if ($currentDataType != $accValue->expectedDataType) {
                    throw new Exception('unexpected Data type '.($currentDataType)." for {$accKey} expect {$accValue->expectedDataType} ".json_encode($response), 409);
                }
            }
        }
    }

    function validateSearchRequest($request, $allowedRequest) { // call to this function must be inside try catch throwable block
        $data = [];
        if ($request) {
            if (isset($request['limit_per_page']) && !is_int($request['limit_per_page'])) {
                throw new Exception("limit_per_page must be integer", 400);
            } elseif (isset($request['current_page']) && !is_int($request['current_page'])) {
                throw new Exception("current_page must be integer", 400);
            } elseif (isset($request['request_columns']) && !is_string($request['request_columns'])) {
                throw new Exception("request_columns must be string", 400);
            }
            
            $data['search'] = $this->cleanSearchRequest($request['search'], $allowedRequest);
            $data['order_by'] = $request['order_by'];
            $data['limit_per_page'] = intval($request['limit_per_page']);
            $data['current_page'] = intval($request['current_page']);
            !empty($data['request_columns'])?
                $data['request_columns'] = explode(",",$request['request_columns']):
                $data['request_columns'] = [];
        } else {
            $data['search'] = [];
            $data['order_by'] = ['_id' => -1];
            $data['limit_per_page'] = 0;
            $data['current_page'] = 1;
            $data['request_columns'] = [];
        }
        return $data;
    }

    private function cleanSearchRequest($search, $allowedRequestList) { // call to this function must be inside try catch throwable block
        if (!empty($allowedRequestList)) {
            $allowedRequest = explode(",", $allowedRequestList);

            foreach ($allowedRequest as $key=>$ar) {
                $allowedRequest[$key] = trim($ar);
            }
            
            $filteredSearch = [];
            if (!empty($search)) {
                foreach ($search as $key=>$value) {
                    if (in_array($key, $allowedRequest)) {
                        $filteredSearch[$key] = $value;
                    }
                }
            }
            $search = $filteredSearch;
        }

        return $search;
    }
}