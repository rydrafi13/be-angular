<?php
use App\RetailerProgram\ViewModels\EvoucherViewModel;

class EvoucherController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'product_code,sku_code,expiry_date,process_number,process_id,type,status,remarks';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $EvoucherViewModel = new EvoucherViewModel();

            $result = $EvoucherViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    public function findSummaryReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'product_code,sku_code,expiry_date,process_number,process_id,type,status,remarks';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $EvoucherViewModel = new EvoucherViewModel();

            $result = $EvoucherViewModel->findReportSummary($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    public function findStockReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'process_number,process_type,status,total_sku,total_qty,total_value,total_price';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $EvoucherViewModel = new EvoucherViewModel();

            $result = $EvoucherViewModel->findStockReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function findMemberEvoucherLists($srv, $params) {
        try {
            $this->authorize("member", "admin");

            $auth = $this->getPayload();
            $option = $srv->get('GET.option');
            $username = $srv->get('GET.username');

            $EvoucherViewModel = new EvoucherViewModel();
            $result = $EvoucherViewModel->findMemberEvoucherLists($auth, $option, $username);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getEvoucherCode($srv, $params) {
        try {
            $this->authorize('member','admin');

            $auth = $this->getPayload();

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'product_code,sku_code,reference_no,expiry_date,process_number,process_id,status,remarks';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $EvoucherViewModel = new EvoucherViewModel();

            $result = $EvoucherViewModel->getEvoucherCode($auth, $data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function getEvoucherDetail($srv, $params) {
        try {
            $this->authorize("member");

            $auth = $this->getPayload();
            $processId = $srv->get('GET.process_id');

            $EvoucherViewModel = new EvoucherViewModel();
            $result = $EvoucherViewModel->getEvoucherDetail($auth, $processId);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function redeemEvoucher($srv, $params) {
        try {
            $this->authorize("member", "admin");
            $auth = $this->getPayload();

            $processId = $params['process_id'];

            $EvoucherViewModel = new EvoucherViewModel();

            $result = $EvoucherViewModel->redeemEvoucher($processId, $auth);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getStockDetail($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $processNumber = $params['process_number'];

            $EvoucherViewModel = new EvoucherViewModel();

            $result = $EvoucherViewModel->getStockDetail($processNumber);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function uploadEvoucherCode($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $EvoucherViewModel = new EvoucherViewModel();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }
            $result = $EvoucherViewModel->uploadEvoucherCode($auth, $file['document']);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function processEvoucherCode($srv, $params) {
        try {
            $this->authorize("admin");

            $this->checkAcceptedRequiredBodyParams([
                'process_number'=>expectedResultArray()->required(),
                'status'=>expectedResultString()->required()
            ]);
            $auth = $this->getPayload();
            $EvoucherViewModel = new EvoucherViewModel();

            $getBody = $this->getBody();
            $result = $EvoucherViewModel->processEvoucherCode($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function injectEvoucherCode($srv, $params) {
        try {
            $this->authorize("admin");

            $auth = $this->getPayload();
            $EvoucherViewModel = new EvoucherViewModel();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }

            $result = $EvoucherViewModel->injectEvoucherCode($auth, $file['document']);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}