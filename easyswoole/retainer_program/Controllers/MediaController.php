<?php
use App\GeneralData\ViewModels\MediaViewModel;

class MediaController extends MasterController{

    function firstLoad(){

    }

    function uploadImage($srv, $params) {
        try {
            $this->authorize("member", "admin");

            $auth = $this->getPayload();

            $MediaViewModel = new MediaViewModel();

            $file = $srv->get("FILES");
            if (empty($file['image'])) {
                throw new Exception("upload image is required", 400);
            }
            $result = $MediaViewModel->uploadImage($auth, $file['image']);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function uploadDocument($srv, $params) {
        try {
            $this->authorize("admin");

            $auth = $this->getPayload();

            $MediaViewModel = new MediaViewModel();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }
            $uploadDocument = $MediaViewModel->uploadDocument($auth, $file['document']);

            $result = ['result'=>$uploadDocument['result']];

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}