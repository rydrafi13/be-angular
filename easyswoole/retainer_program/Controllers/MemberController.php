<?php
use App\RetailerProgram\ViewModels\MemberViewModel;

class MemberController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowedRequest = 'username,full_name,cell_phone,email,status,gender,created_date,point_balance,input_form_data';
            foreach ($request['search'] as $key => $value) {
                if (strpos($key, '.') !== false){
                    $allowedRequest .= ",{$key}";
                }
            }

            $data = $this->validateSearchRequest($request, $allowedRequest);

            $MemberViewModel = new MemberViewModel();

            $result = $MemberViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");
            $request = json_decode($srv->get('GET.request'), 1);
            
            $search = $request['search'] ?: [];

            $MemberViewModel = new MemberViewModel();

            $result = $MemberViewModel->findMember($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getMemberDetail($srv, $params) {
        try {
            $this->authorize("member");
            $auth = $this->getPayload();

            $MemberViewModel = new MemberViewModel();

            $result = ['result'=> $MemberViewModel->getMemberDetail($auth)];

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function changeMemberPassword($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $this->checkAcceptedRequiredBodyParams([
                'username'=>expectedResultArray()->required()
            ]);

            $getBody = $this->getBody();
            $MemberViewModel = new MemberViewModel();
            
            $result = $MemberViewModel->changeMemberPassword($auth, $getBody['username']);


            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function registerNewMember($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $getBody = $this->getBody();

            $MemberViewModel = new MemberViewModel();
            
            $result = $MemberViewModel->memberRegistration($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function registerNewMemberBulk($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }

            $MemberViewModel = new MemberViewModel();
            $result = $MemberViewModel->memberRegistrationBulk($auth, $file['document']);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateMember($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $getBody = $this->getBody();
            $MemberViewModel = new MemberViewModel();

            $result = $MemberViewModel->updateByInputForm($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateMemberBulk($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $MemberViewModel = new MemberViewModel();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }
            $result = $MemberViewModel->updateByInputFormBulk($auth, $file['document']);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function deleteMember($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();
            
            $username = $params['username'];

            $MemberViewModel = new MemberViewModel();

            $result = $MemberViewModel->deleteMember($username);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function markStatusComplete($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();
            
            $this->checkAcceptedRequiredBodyParams([
                "member_id"=>expectedResultArray()->required(),
                "status"=>expectedResultString()->required()
            ]);
            
            $getBody = $this->getBody();

            $MemberViewModel = new MemberViewModel();
            $result = $MemberViewModel->markStatusComplete($auth, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateAdditionalInfo($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();
            
            $method = $params['method'];
            $getBody = $this->getBody();

            $MemberViewModel = new MemberViewModel();

            $result = $MemberViewModel->updateAdditionalInfo($auth, $method, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}