<?php

use App\RetailerProgram\ViewModels\CategoryViewModel;

class CategoryController extends MasterController
{
    public function findAllReport($srv, $params){

        
        try {
            $this->authorize('admin');
            
            $request = $srv->get("GET.request");
            $request = json_decode($request, 1);
            $allowed_request = "type,name,code,key";
            
            $data = $this->validateSearchRequest($request, $allowed_request);
            $CategoryViewModel = new CategoryViewModel();
            $result = $CategoryViewModel->find($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page']?$data['limit_per_page']:50, null);
            
            return $this->sendResult($result);
        } catch (Exception $e) {
            $this->sendError($e);
        }
        
    }
    
    public function find($srv, $params)
    {
        try {
            $this->authorize('admin','member');
            $request = json_decode($srv->get('GET.request'), 1);
            $search = $request['search'] ?: [];
            
            $CategoryViewModel = new CategoryViewModel();
            $result = $CategoryViewModel->find($search);
            
            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
        
    }
    
    public function getCategoryList($srv)
    {
        try {
            $this->authorize('admin','member');
            $auth = $this->getPayload();

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);

            $CategoryViewModel = new CategoryViewModel();

            $result = $CategoryViewModel->getCategoryList($request['search']?:[]);
            return $this->sendResult($result);

        } catch(Throwable $e){
            return $this->sendError($e);
        }
    }
    
    public function remove($srv, $params)
    {
        $keyOrCode = $params['key_or_code'];

        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $categoryViewModel = new CategoryViewModel();
            
            $result = $categoryViewModel->deleteCategory($auth, $keyOrCode);
            
            return $this->sendResult($result);

        } catch(Throwable $e){
            return $this->sendError($e);
        }
    }

    public function update($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $keyOrCode = $params['key_or_code'];
            $getBody = $this->getBody();

            $CategoryViewModel = new CategoryViewModel();

            $result = $CategoryViewModel->updateCategory($auth, $keyOrCode, $getBody);

            return $this->sendResult($result);

        } catch(Throwable $e){
            return $this->sendError($e);
        }
    }

    public function insert($srv)
    {
        try{
            $this->authorize('admin');
            $auth = $this->getPayload();
    
            $this->checkAcceptedRequiredBodyParams([
                'name'=>expectedResultString()->required(),
                'type'=>expectedResultString()->required(),
                'code'=>expectedResultString()->required(),
                'key'=>expectedResultString()->required()
            ]);
            $getBody = $this->getBody();
    
            $CategoryViewModel = new CategoryViewModel();
    
            $result = $CategoryViewModel->insertCategory($auth, $getBody);

            return $this->sendResult($result);

        } catch(Throwable $e){
            return $this->sendError($e);
        }
    }
}
