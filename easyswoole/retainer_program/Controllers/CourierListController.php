<?php

use App\GeneralData\ViewModels\CourierViewModel;

class CategoryController extends MasterController
{
    public function findAllReport($srv, $params){

        
        try {
            $this->authorize('admin');
            
            $request = $srv->get("GET.request");
            $request = json_decode($request, 1);
            $allowed_request = "type,name,code,key";
            
            $data = $this->validateSearchRequest($request, $allowed_request);
            $CourierViewModel = new CourierViewModel();
            $result = $CourierViewModel->find($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page']?$data['limit_per_page']:50, null);
            
            return $this->sendResult($result);
        } catch (Exception $e) {
            $this->sendError($e);
        }
        
    }
    
    public function find($srv, $params)
    {
        try {
            $this->authorize('admin','member');
            $request = json_decode($srv->get('GET.request'), 1);
            $search = $request['search'] ?: [];
            
            $CourierViewModel = new CourierViewModel();
            $result = $CourierViewModel->find($search);
            
            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
        
    }

    public function getAvailableCourier($srv, $params)
    {
        try {
            $originCode = $srv->get('GET.origin_code');
            $destinationCode = $srv->get('GET.destination_code');
            
            $CourierViewModel = new CourierViewModel();
            $result = $CourierViewModel->getAvailableCourier($originCode, $destinationCode);
            
            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
    
    public function remove($srv, $params)
    {
        $keyOrCode = $params['key_or_code'];

        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $CourierViewModel = new CourierViewModel();
            
            $result = $CourierViewModel->deleteCategory($auth, $keyOrCode);
            
            return $this->sendResult($result);

        } catch(Throwable $e){
            return $this->sendError($e);
        }
    }

    public function update($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $keyOrCode = $params['key_or_code'];
            $getBody = $this->getBody();

            $CourierViewModel = new CourierViewModel();

            $result = $CourierViewModel->updateCategory($auth, $keyOrCode, $getBody);

            return $this->sendResult($result);

        } catch(Throwable $e){
            return $this->sendError($e);
        }
    }

    public function insert($srv)
    {
        try{
            $this->authorize('admin');
            $auth = $this->getPayload();
    
            $this->checkAcceptedRequiredBodyParams([
                'name'=>expectedResultString()->required(),
                'type'=>expectedResultString()->required(),
                'code'=>expectedResultString()->required(),
                'key'=>expectedResultString()->required()
            ]);
            $getBody = $this->getBody();
    
            $CourierViewModel = new CourierViewModel();
    
            $result = $CourierViewModel->insertCategory($auth, $getBody);

            return $this->sendResult($result);

        } catch(Throwable $e){
            return $this->sendError($e);
        }
    }
}
