<?php
use App\RetailerProgram\ViewModels\PointsTransactionViewModel;

class PointsTransactionController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'user_id,item_code,reference_no,description,remarks,status,transaction_month,item_code,process_type,process_number,process_id,created_date,updated_date,based_on,id_toko,nama_toko,nama_pemilik,no_wa_pemilik,group';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $PointsTransactionViewModel = new PointsTransactionViewModel();

            $result = $PointsTransactionViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");
            $request = json_decode($srv->get('GET.request'), 1);
            
            $search = $request['search'] ?: [];

            $PointsTransactionViewModel = new PointsTransactionViewModel();

            $result = $PointsTransactionViewModel->find($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function newPointsTransaction($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            }

            $processType = $srv->get('GET.process_type');

            $PointsTransactionViewModel = new PointsTransactionViewModel();
            $result = ['result'=> $PointsTransactionViewModel->newPointsTransaction($auth, $file['document'], $processType)];

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function processPointsTransaction($srv, $params) {
        try {
            $this->authorize("admin");
            $auth = $this->getPayload();

            $this->checkAcceptedRequiredBodyParams([
                'process_number'=>expectedResultString()->required(),
                'status'=>expectedResultString()->required()
            ]);

            $getBody = $this->getBody();
            $PointsTransactionViewModel = new PointsTransactionViewModel();
            
            $result = $PointsTransactionViewModel->processPointsTransaction($auth, $getBody);


            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}