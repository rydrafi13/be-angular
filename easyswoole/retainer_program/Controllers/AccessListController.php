<?php
use App\UserManagement\ViewModels\AccessListViewModel;

class AccessListController extends MasterController{

    function firstLoad(){

    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");

            $request = json_decode($srv->get('GET.request'), 1);
            $search = $request['search'] ?: [];

            $AccessListViewModel = new AccessListViewModel();

            $result = $AccessListViewModel->findAccessList($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getAccessListByApp($srv, $params) {
        try {
            $this->authorize("admin");

            $AccessListViewModel = new AccessListViewModel();

            $result['result'] = $AccessListViewModel->getAccessListByApp();

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function addNewAccessMenu($srv, $params) {
        try {
            // $this->authorize("god_mode");
            $this->authorize("admin");
            $auth = $this->getPayload();

            $options = $srv->get('GET.option');

            switch ($options) {
                case 'single':
                    $this->checkAcceptedRequiredBodyParams([
                        'name'=>expectedResultString()->required(),
                        'endpoint'=>expectedResultString()->required(),
                        'app_label'=>expectedResultString()->required()
                    ]);
                    $bodyData = $this->getBody();
                    break;
                
                case 'bulk':
                    $file = $srv->get("FILES");
                    if (empty($file['document'])) {
                        throw new Exception("upload document is required", 400);
                    }
                    $bodyData = $file['document'];
                    break;
            }

            $AccessListViewModel = new AccessListViewModel();

            $result = $AccessListViewModel->addNewAccess($auth, $options, $bodyData);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateAccessMenu($srv, $params) {
        try {
            $this->authorize("god_mode");

            $auth = $this->getPayload();
            $accessName = $srv->get('GET.name');
            $appLabel = $srv->get('GET.app_label');
            $getBody = $this->getBody();

            $AccessListViewModel = new AccessListViewModel();

            $filter = [
                'name'=>$accessName,
                'app_label'=>$appLabel
            ];
            $result = $AccessListViewModel->updateAccess($filter, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function deleteAccessMenu($srv, $params) {
        try {
            $this->authorize("god_mode");
            
            $auth = $this->getPayload();
            $accessName = $srv->get('GET.name');
            $appLabel = $srv->get('GET.app_label');

            $AccessListViewModel = new AccessListViewModel();

            $filter = [
                'name'=>$accessName,
                'app_label'=>$appLabel
            ];
            $result = $AccessListViewModel->deleteAccess($filter);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

}