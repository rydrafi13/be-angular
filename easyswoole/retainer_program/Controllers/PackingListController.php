<?php
use App\RetailerProgram\ViewModels\PackingListViewModel;

class PackingListController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowedRequest = 'packing_no,courier_code,courier_name,delivery_method,delivery_service,status,delivery_date';

            $data = $this->validateSearchRequest($request, $allowedRequest);

            $PackingListViewModel = new PackingListViewModel();

            $result = $PackingListViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function createPackingList($srv, $params) {
        try {
            $this->authorize('admin');

            $auth = $this->getPayload();
            $PackingListViewModel = new PackingListViewModel();

            $this->checkAcceptedRequiredBodyParams([
                'reference_no' => expectedResultArray()->required()
            ]);

            $getBody = $this->getBody();

            $result = $PackingListViewModel->createPackingList($auth, $getBody['reference_no']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function removePackingList($srv, $params) {
        try {
            $this->authorize('admin');

            $auth = $this->getPayload();
            $PackingListViewModel = new PackingListViewModel();

            $this->checkAcceptedRequiredBodyParams([
                'packing_no' => expectedResultString()->required(),
                'reference_no' => expectedResultArray()->required()
            ]);

            $getBody = $this->getBody();

            $result = $PackingListViewModel->removePackingList($auth, $getBody['packing_no'], $getBody['reference_no']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function processDeliveryPackingList($srv, $params) {
        try {
            $this->authorize('admin');

            $auth = $this->getPayload();
            $PackingListViewModel = new PackingListViewModel();

            $this->checkAcceptedRequiredBodyParams([
                'packing_no' => expectedResultString()->required()
            ]);

            $getBody = $this->getBody();

            $result = $PackingListViewModel->processDeliveryPackingList($auth, $getBody['packing_no']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }
}