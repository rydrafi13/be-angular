<?php
use App\UserManagement\ViewModels\AdminGroupViewModel;

class AdminGroupController extends MasterController{

    function firstLoad(){

    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");

            $request = json_decode($srv->get('GET.request'), 1);

            $search = $request['search'] ?: [];

            $AdminGroupViewModel = new AdminGroupViewModel();

            $result = $AdminGroupViewModel->findAdminGroup($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function addNewAdminGroup($srv, $params) {
        try {
            $this->authorize("admin");

            $auth = $this->getPayload();
            $getBody = $this->getBody();

            $AdminGroupViewModel = new AdminGroupViewModel();

            $result = $AdminGroupViewModel->addNewGroup($getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateAdminGroup($srv, $params) {
        try {
            $this->authorize("admin");

            $auth = $this->getPayload();
            $groupName = $params['group_name'];
            $getBody = $this->getBody();

            $AdminGroupViewModel = new AdminGroupViewModel();

            $result = $AdminGroupViewModel->updateGroup($groupName, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function deleteAdminGroup($srv, $params) {
        try {
            $this->authorize("admin");
        
            $auth = $this->getPayload();
            $groupName = $params['group_name'];

            $AdminGroupViewModel = new AdminGroupViewModel();

            $result = $AdminGroupViewModel->deleteAdminGroup($groupName);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

}