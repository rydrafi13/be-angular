<?php

function loadClass() {

    @spl_autoload_register(
        function($class) {
            $namespace = explode("\\", trim($class));
            
            if ($namespace[0] == "App") {
                $currentDir     = PROJECT_PATH.$namespace[0].DIRECTORY_SEPARATOR.$namespace[1].DIRECTORY_SEPARATOR.$namespace[2];
                if (!is_dir($currentDir)) return;

                if (strpos($namespace[2],'ViewModels') !== false) {
                    if (strpos($namespace[3],'ViewModelMaster') !== false) {
                        $currentFile    = $currentDir.DIRECTORY_SEPARATOR.$namespace[3].'.php';
                    } else {
                        $currentFile    = $currentDir.DIRECTORY_SEPARATOR.$namespace[3].DIRECTORY_SEPARATOR.$namespace[3].'.php';
                    }
                } elseif (strpos($namespace[2],'Models') !== false) {
                    $currentFile    = $currentDir.DIRECTORY_SEPARATOR.$namespace[3].'.php';
                }
            } else if($namespace[0] == "Services") {
                $currentDir     = PROJECT_PATH.$namespace[0].DIRECTORY_SEPARATOR.$namespace[1];
                if (!is_dir($currentDir)) return;

                $currentFile    = $currentDir.DIRECTORY_SEPARATOR.$namespace[1].'.php';
            } else {
                $currentDir     = PROJECT_PATH.$namespace[0];
                if (!is_dir($currentDir)) return;

                $currentFile    = $currentDir.DIRECTORY_SEPARATOR.$namespace[1].'.php';
            }

            if (empty($currentFile) || !file_exists($currentFile)) return;

            @include_once($currentFile);

            /* if(strpos($className,'ViewModelMaster') !== false){
                $stringFileLocation = ($currentDir.DIRECTORY_SEPARATOR."viewmodels/".$className.".php");
            } elseif(strpos($className,'ViewModel') !== false){
                $stringFileLocation = ($currentDir.DIRECTORY_SEPARATOR."viewmodels/{$className}/".$className.".php");
            } elseif(strpos($className,'Model') !== false){
                $stringFileLocation = ($currentDir.DIRECTORY_SEPARATOR."models/".$className.".php");
            }
    
            if(!class_exists($className) && isset($stringFileLocation) && file_exists($stringFileLocation)){
                @include_once($stringFileLocation);
            } */
        }
    );

}

/*
 * @param $message The request Message should be in String
 * @param $log_type by date_default_timezone_get()lt is using DEBUG. The Others Logs type is like TRACE, DEBUG, INFO, WARN, ERROR, FATAL. /ref log-message
 */
   
function write_log($message='', $log_type='DEBUG')
{
    global $requestSession, $srv;
    //var_dump($srv->get("BODY"));
    if (!defined("LOG_DIR") || !defined("LOG_ENABLED") || LOG_ENABLED!==true) {
        return false;
    }
        
    $logType    = strtoupper($log_type);
    $curTime    = time();
    $curYMD     = date("Y-m-d", $curTime);
    $curYMDH    = date("Y-m-d-H");
    $dir        = LOG_DIR.$log_type.DIRECTORY_SEPARATOR.$curYMD.DIRECTORY_SEPARATOR;
    $old        = umask(0);

    if (!is_dir('.'.DIRECTORY_SEPARATOR.$dir)) {
        @mkdir($dir, '0755', true);
    }
    umask($old);
    
    $returnResult = array();
    $returnResult['REQ_ID']     = $requestSession;
    $returnResult['REQ_IP_ADDRESS'] = getClientIpAddr();
    $returnResult['LOG_TYPE']   = $log_type;

    if (is_string($message)) {
        $returnResult = array_merge($returnResult, array('msg'=>$message));
    } elseif (is_array($message)) {
        $returnResult =  array_merge($returnResult, $message);
    }

    $data = date('r').trim(json_encode($returnResult)).PHP_EOL;
    file_put_contents($dir.$curYMDH.'.log', $data ,LOCK_EX|FILE_APPEND);
}

function getClientIpAddr() {
    $ipaddress = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(!empty($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(!empty($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(!empty($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(!empty($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = '0.0.0.0';
    return $ipaddress;
}

function createDateTime($date, $mode = 'datetime', $format = 'Y-m-d H:i:s', $tz = TZ)
{
    $timezone = timezone_open($tz);
    if ($timezone == false) {
        throw new Exception("invalid time zone {$tz}", 400);
    }

    $d = date_create($date, $timezone);
    if ($d == false) {
        throw new Exception("invalid date {$date}", 400);
    }

    switch ($mode) {
        case 'format':
            $result = $d->format($format);
            break;
        
        case 'timestamp':
            $result = $d->getTimestamp();
            break;

        case 'datetime':
            $result = $d;
            break;
    }

    return $result;
}

class ExpectedResult
{
    public function required()
    {
        $this->required = true;
        return $this;
    }

    public function setExpectedTypeData($dataType)
    {
        $this->expectedDataType = $dataType;
        return $this;
    }
}

function expectedResult($dataType)
{
    $resultObject = new ExpectedResult();
    $resultObject->expectedDataType = $dataType;
    return $resultObject;
}

function expectedResultString()
{
    return expectedResult('string');
}

function expectedResultNumber()
{
    return expectedResult('integer');
}

function expectedResultDouble()
{
    return expectedResult('double');
}

function expectedResultArray()
{
    return expectedResult('array');
}

function returnShouldNotEmptyArray()
{
    return expectedResult('notEmptyArray');
}

function expectedResultObject()
{
    return expectedResult('object');
}

function convertSecondsTodhMS($time, $currentTime=0)
{
    $seconds = $time;
    if ($currentTime != 0) {
        $seconds = $currentTime - $time;
    }
    
    $string = "";
    $dt1    = new DateTime("@0");
    $dt2    = new DateTime("@$seconds");
    $days   = $dt1->diff($dt2)->format('%a');

    if ($days !== "0" && $days!=1) {
        $string.= " {$days} days";
    } elseif ($days == "1") {
        $string.= " {$days} day";
    }

    $hours = $dt1->diff($dt2)->format('%h');

    if ($hours !== "0" && $hours!=1) {
        $string.= " {$hours} hours";
    } elseif ($hours == "1") {
        $string.= " {$hours} hour";
    }

    $minutes = $dt1->diff($dt2)->format('%i');

    if ($minutes !== "0" && $minutes!=1) {
        $string.= " {$minutes} min";
    }
   

    $seconds = $dt1->diff($dt2)->format('%s');
    if ($seconds !== "0" && $seconds!=1) {
        $string.= " {$seconds} sec";
    }

    return $string;
}

/*! \file
* \fn applyHooks($hookName, $arguments) 
* \brief This Function is commonly used for Apply hooker from the other View Models. 
It will finding the `ViewModels/Functions/*{HookerName}{priorityNumber}.php`. So, if you want to sort it by priority you can give name as like `**OnAfterRegister1.php`. It will be first priority `"Ordered by ASC"`

Example:

``$return = applyHooks("OnRegister", ['data1', 'data2']);``

it will call All of **ViewModels/{EachViewModelDirectory}/{ViewModel}OnRegister{$priority}.php** <br/>
['data1', 'data2'] will be always passed trough each Functions that called.<br/>
for the Function priorty sequence More than 1 , the last result data will be always passed in the latest arguments.<br/>
so the  

1. {ViewModel}OnRegister2.php
2. {ViewModel}OnRegister3.php
3. {ViewModel}OnRegister4.php
4. etc

will receive the pass parameters from the previous Function call as Latest arguments<br/>
as example above arguments should be ['data1', 'data2', 'latestreturn' ]<br/>
and return the latest return of latest excuted priority of functions<br/>
* \param $hookName (String) It is the name of Functions of Hooker e.g. : OnRegister, OnAfterRegister, It always using "On" as prefix. 
* \param $arguments (Array) is the value that you want to pass in the ViewModel/Functions e.g it will turn like this `AuthenticationViewModel->AuthenticationOnUserRegister($arguments[0], $arguments[1], $arguments[2], .... )`
* \return the return result is not specifics based on the return of the "***On{HookerName}" it self
*/

function applyHooks($hookName, $arguments){
   
    if(!is_array($arguments)) return "applyHooks, $arguments NEED TO BE ARRAY";

    $findHookList = findHooksList($hookName);
    $return = null;

    foreach($findHookList as $hooker){
        $newObj = new $hooker['class']();

        $argArr=[];
        foreach($arguments as $key=>$arg){
            $argArr[]="\$arguments[$key]";
        }

        $argStr = implode(",", $argArr);
        eval("\$returnObj = \$newObj->{$hooker['function']}($argStr, \$return);");
        $return = $returnObj;
    }
    
    return $return;
}

function findHooksList($hookName){
    chdir('app');
    $appList = glob('*', GLOB_ONLYDIR);

    foreach ($appList as $k => $app) {
        if (!is_dir($app) || !is_dir($app.DIRECTORY_SEPARATOR.'viewmodels')) continue;

        // $pos = strrpos($app, DIRECTORY_SEPARATOR);
        // $appName = substr($app, $pos+1);
        $appName = $app;
        chdir($appName.DIRECTORY_SEPARATOR.'viewmodels');

        $vmdirLoadedBack = false;
        //reset back the directory
        $viewModelList = glob('*', GLOB_ONLYDIR);

        $listedClassAndFunction = [];

        foreach($viewModelList as $sk => $viewModel){

            if(!is_dir($viewModel)){
                chdir('viewmodels');
            }

            $listOfFunctions = glob($viewModel."/functions/*$hookName*.php");
            foreach($listOfFunctions as $key => $function){
                $strFunction  = preg_replace('/'.$viewModel.'\/functions\/(.*)\.php/', '$1', $function);

                if(!$vmdirLoadedBack || !is_dir('viewmodels')){
                    chdir('../');
                    $vmdirLoadedBack = true;
                }
                loadViewModel($appName, $viewModel);
                preg_match('/[0-9]+/', $strFunction, $output_array);
                $priority = 0;

                if(count($output_array) > 0) {
                    $priority = $output_array[0];
                }
                
                $listedClassAndFunction[$priority.$viewModel.$strFunction] = ['class'=>$viewModel, 'function'=>$strFunction];

                // break;
                // $function = str_replace($viewModel."/functions/", "");
            }
        }

        if (!is_dir($appName)) 
            chdir('../');
    }

    if(!is_dir('app')){
        chdir('../');
    }
    
    ksort($listedClassAndFunction);
    
    return $listedClassAndFunction;
}

/* function routesJSONLoader(){
    global $f3;
    $routerList     = glob('routes/JSONs/*.json');
    foreach($routerList as $routerJson){
        $jsonString     = @file_get_contents($routerJson);
        $jsonObject     = json_decode($jsonString,1);
        
        foreach($jsonObject as $router){
           $rt = $router['method']." ".$router['version_path'].$router['route_url'];
           $f3->route( $rt,  $router['class_method']);
        }
    }
    
} */

function convertToUTCMongoBSONDateTime($dateInString, $dayTime = 'start', $tzFrom = 'Asia/Jakarta', $convertHMS = true){
    $hms = '';
    if($dayTime == null){
        $dayTime = 'start';
    }
    if($tzFrom == null){
        $tzFrom = 'Asia/Jakarta';
    }
    
    if($dayTime == 'start' && $convertHMS == true){
        $hms = " 00:00:00 ";
    }
    elseif($dayTime == 'end' && $convertHMS == true ){
        $hms = ' 23:59:59';
    }

    $date = new DateTime($dateInString.$hms, new DateTimeZone($tzFrom));
    $tz   = new DateTimeZone('UTC');

    $date->setTimeZone($tz);
    $timestamp = $date->format('U');

    $from = new MongoDB\BSON\UTCDateTime($timestamp*1000);
    return $from;
}

function convertToLocalMongoBSONDateTime($dateInString, $dayTime = 'start', $tzFrom = 'Asia/Jakarta', $convertHMS = true){
    $hms = '';
    if($dayTime == null){
        $dayTime = 'start';
    }
    if($tzFrom == null){
        $tzFrom = 'Asia/Jakarta';
    }
    
    if($dayTime == 'start' && $convertHMS == true){
        $hms = " 00:00:00 ";
    }
    elseif($dayTime == 'end' && $convertHMS == true ){
        $hms = ' 23:59:59';
    }
    //Make sure the data is in Date Format YYYY-MM-DD
    if($dateInString < "1970-01-01") $dateInString = "1970-01-01";
    
    $dateInString = date("Y-m-d", strtotime($dateInString));

    $date = new DateTime($dateInString.$hms, new DateTimeZone($tzFrom));

    // $date->setTimeZone($tz);
    $timestamp = $date->format('U');

    $from = new MongoDB\BSON\UTCDateTime($timestamp*1000);
    return $from;
}

function getClientInfo(){

    $clientInfo = get_browser(null, true);
    
    $returnData = [
        'browser' => "{$clientInfo['browser']} {$clientInfo['version']}",
        'platform' => $clientInfo['platform'],
        'ip_address' => getClientIpAddr()
    ];

    return $returnData;
}

function getDeviceInfo ($body) {

    $returnResult = [];
    if (isset($body['device_id'])) {
        $returnResult['device_id'] = $body['device_id'];
    }
    if (isset($body['platform'])) {
        $returnResult['platform'] = $body['platform'];
    }
    if (isset($body['device_model'])) {
        $returnResult['device_model'] = $body['device_model'];
    }

    $result['device_info'] = $returnResult;
    return $result;
}

function generateAlphaNumericString($charLength = 6, $mode = 1, $charOccurence = false) {

    $stringData = '';
    switch ($mode) {
        case 1:
            $stringData .= implode(range(0, 9));
            break;
        
        case 2:
            $stringData .= implode(range('a', 'z'));
            $stringData .= implode(range('A', 'Z'));
            $stringData .= implode(range(0, 9));
            break;

        case 3:
            $stringData .= implode(range('A', 'Z'));
            $stringData .= implode(range(0, 9));
            break;
    }

    if($charLength > strlen($stringData)) $charLength = strlen($stringData);

    $count = $charLength;
    $generatedString = '';
    $usedChar = [];
    foreach (range(0, $count-1) as $key => $value) {
        $char = $stringData[random_int(0, strlen($stringData) - 1)];
        $generatedString .= $char;
        if ($charOccurence != false && is_numeric($charOccurence)){
            $charOccurence = intval($charOccurence);
            $usedChar[$char] = ($usedChar[$char]?:0) + 1;

            if($usedChar[$char] == $charOccurence) $stringData = str_replace($char, '', $stringData);

        }
    }

    return $generatedString;
}