<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/5/24
 * Time: 下午3:43
 */

namespace EasySwoole\Http;

use EasySwoole\Http\Request;
use EasySwoole\Http\Response;

class Srv{
    public $HEADERS;
    public $GET;
    public $POST;
    public $BODY;
    public $FILES;
    public $SERVER;
    public $query_params;
    public $request_method;
    public $request_uri;
    public $server_method;

    function get($name){
        if(isset($this->{$name})){
            return $this->{$name};
        }
        else{
            if(strpos($name, "GET") == "0"){
                $exp = explode(".", $name);
                if(!isset($this->{$exp[0]}[$exp[1]])){
                    return null;
                }
                else {
                    return $this->{$exp[0]}[$exp[1]];
                }
            }
        }
    }

    function setRequest($request) {

        $_SERVER['HTTP_USER_AGENT'] = $request->request->header['user-agent'];
        $_SERVER['HTTP_CLIENT_IP'] = $request->request->server['remote_addr'];

        $this->request_method = $request->request->server['request_method'];
        $this->request_uri    = $request->request->server['request_uri'];
        $this->server_method  = $request; 
        $this->query_params    = $request->getQueryParams();

        $this->BODY    = $request->getBody();
        $this->HEADERS = $request->request->header;
        $this->GET     = $request->request->get;
        $this->POST    = $request->request->post;
        $this->FILES   = $request->request->files;
        $this->SERVER  = $request->require->server;
    }

    function setResponse($response){

    }
}

class Bridge
{
    public $Request ;
    public $Response ;
    public $Srv;

   function __construct(Request $request, Response $response){
       $this->Request = $request;
       
       $this->Response = $response;
       $this->prepare();
   }
   function getDataResponse(){
    //    var_dump($this->Response);
        return $this->Response;
   }
   function prepare(){
       $this->Srv = new Srv();
       $this->Srv->setRequest($this->Request);
       $this->Srv->setResponse($this->Response);
   }
}

