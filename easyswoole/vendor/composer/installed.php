<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-stagging',
    'version' => 'dev-stagging',
    'aliases' => 
    array (
    ),
    'reference' => 'bdde11d352db0c1f626c76ad57fec63ed09abeaf',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-stagging',
      'version' => 'dev-stagging',
      'aliases' => 
      array (
      ),
      'reference' => 'bdde11d352db0c1f626c76ad57fec63ed09abeaf',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '72b6fbf76adb3cf5bc0db68559b33d41219aba27',
    ),
    'easyswoole/annotation' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '89d586ae21b0ccdcac48bf36e0599077b3b7445b',
    ),
    'easyswoole/component' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eeca15d97a475424859662ca856a0ca2986a7ec1',
    ),
    'easyswoole/config' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7d2754ed63c961d53ed33a69fb92de26e4914e26',
    ),
    'easyswoole/easyswoole' => 
    array (
      'pretty_version' => '3.3.4',
      'version' => '3.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d73bdb1d7884ef629992fc95f7c0a59519231b4',
    ),
    'easyswoole/http' => 
    array (
      'pretty_version' => '1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '2f320470c4900955009ead4512de3ecefb7264f9',
    ),
    'easyswoole/log' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9f7a8c139523db90992f89a91f2714413f6d637',
    ),
    'easyswoole/spl' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd0fb72ee0a9febf64a241ebc1cbcaad47d657570',
    ),
    'easyswoole/task' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a58f85a8b2f552b805c62c99904d4f25940d868d',
    ),
    'easyswoole/template' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '59a18706190276a58878a51118441e88ba5df361',
    ),
    'easyswoole/trigger' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f449f974d6645342134f455ce4a6885c8ad3a92e',
    ),
    'easyswoole/utility' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '585252be9ed0cdfd8badbddb7df60edce794daf0',
    ),
    'easyswoole/validate' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6caafd4ceef47b487e0f0617cad85c39ededc635',
    ),
    'jeremeamia/superclosure' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5707d5821b30b9a07acfb4d76949784aaa0e9ce9',
    ),
    'nikic/fast-route' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a9981c347c5c49d6dfe5cf826bb882b824080dc',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.14.0',
      'version' => '1.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '16ec91cb06998b609501b55b7177b7d7c02badb3',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.14.0',
      'version' => '1.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba3cfcea6d0192cae46c62041f61cbb704b526d3',
    ),
  ),
);
